# -*- coding: utf-8 -*-
"""
Created on Sun May  8 01:11:49 2016

@author: Yorick
"""

"""Fichier d'installation de notre script salut.py."""

from cx_Freeze import setup, Executable
import sys
import os
from cx_Freeze import hooks
#import scipy

#def load_scipy_patched(finder, module):
#    """the scipy module loads items within itself in a way that causes
#        problems without the entire package and a number of other subpackages
#        being present."""
#    finder.IncludePackage("scipy._lib")  # Changed include from scipy.lib to scipy._lib
#    finder.IncludePackage("scipy.misc")
##
##hooks.load_scipy = load_scipy_patched
dirpath='E:\\YORICK\\SOFT_MAISON\\CROSSTONE'

includefiles_list=[]


includefiles_list=[dirpath+'\\source\\save_button.png',dirpath+'\\source\\logo_crosstone.png',dirpath+'\\source\\play_icone_imag.png',dirpath+'\\source\\stop_icon_imag.png',dirpath+'\\source\\splash_crosstone.png']
#scipy_path ='C:\\python_27\\python-2.7.10.amd64\\Lib\\site-packages\\scipy\\io'
#includefiles_list.append('C:\\python_27\\python-2.7.10.amd64\\Lib\\site-packages\\scipy\\special\\_ufuncs.pyd')
#includefiles_list.append('C:\\python_27\\python-2.7.10.amd64\\Lib\\site-packages\\scipy\\special\\_ufuncs_cxx.pyd')

# exclude unneeded packages. More could be added. Has to be changed for
# other programs.
excludes=['Tkinter', 'zmq','matplotlib','collections.abc','scipy']
optimize=2
#excludes=['collections.abc']

#bin_includes = ["pywintypes34.dll"]

bin_includes = []
packages=[]
includes = ["sip","re","atexit"]
build_exe_options = {"packages":packages,"optimize":optimize,"include_files":includefiles_list,"includes":includes,"excludes":excludes, "bin_includes": bin_includes}
# Information about the program and build command. Has to be adjusted for
# other programs
setup(
    name="Crosstone",                           # Name of the program
    version="1.0",                              # Version number
    description="FRF Analysis",     # Description
    author="Yorick Buot de l'Epine",
    author_email="ybuotdel@gmail",
    options = {"build_exe": build_exe_options}, # <-- the missing line
    executables=[Executable("crosstone.py",
                            icon='logo_crosstone.ico',# Executable python file
                            compress=True,
                            base = ("Win32GUI" if sys.platform == "win32" 
                            else None))],
)
