# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 16:33:32 2015

@author: Yorick
"""

import numpy
import time as temps
#import pyqtgraph as pg
import pyaudio
import os.path
####################################
from PyQt5 import QtCore, QtGui
import sys
import crosstone_gui
from class_PLOT import plot_monitorL,plot_monitorR,plot_autospectrum,plotco,plot2D
import psutil

def kill_proc_tree(pid, including_parent=True):
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    if including_parent:
        parent.kill()

class Variable:
    def __init__(self):
        self.freq_max = [] #max frequency to study
        self.ratio=2.56
        self.buffer_size = [] # size of buffer
        self.delta_freq=[] #
        self.window=[] # windowed type
        self.covering=[] #covering for windowed hanning or exponential
        self.covering2=[] #covering for windowed Force impact
        self.channel1_sensitivity=[]
        self.channel2_sensitivity=[]
        self.input1_type=[]
        self.input1_parameter=[]
        self.ref_db=[]
        self.time_range=[]
        self.nb_mean=[]
#        self.range=[]
        self.data_freq=[]

    def clear(self):
        self.freq_max = [] #max frequency to study
        self.ratio=2.56
        self.buffer_size = [] # size of buffer
        self.delta_freq=[] #
        self.window=[] # windowed type
        self.covering=[] #covering for windowed hanning or exponential
        self.covering2=[] #covering for windowed Force impact
        self.channel1_sensitivity=[]
        self.channel2_sensitivity=[]
        self.input1_type=[]
        self.input1_parameter=[]
        self.ref_db=[]
        self.nb_mean=[]
        self.time_range=[]
#        self.range=[]
        self.data_freq=[]

class splashScreen:
    def __init__(self):
        self.splash=QtGui.QWidget()
        splash_pix = QtGui.QPixmap(os.getcwd()+'\splash_crosstone.png')
        self.splash.setFixedSize(splash_pix.size())
        self.splash.setWindowFlags(QtCore.Qt.FramelessWindowHint |
                            QtCore.Qt.WindowStaysOnTopHint)
        palette = QtGui.QPalette()
        palette.setBrush(10, QtGui.QBrush(splash_pix))                     # 10 = Windowrole
        self.splash.setPalette(palette)

    def show_splash(self):
        self.splash.show()
    def close_splash(self):
        self.splash.close()


class signal_BUFF:
    def init(self,signal_size):
        self.data=1e-7*numpy.ones((signal_size))
    def add(self,x):
        self.data[0:numpy.size(x)]=x
        self.data=numpy.roll(self.data,-numpy.size(x))
    def reset(self):
        self.data=1e-7*numpy.ones((numpy.size(self.data)))

def fenetre(poids,bloc):
    bloc_out=numpy.multiply(poids,bloc)
    return bloc_out


def autospectrum(signal_freq) :
#signal_freq= tableaux contenant les FFTs fois le nombre de colonne
#    auto_spect=numpy.zeros((numpy.shape(signal_freq)[0]),dtype=int)
    auto_spect=numpy.abs(numpy.conj(signal_freq)*signal_freq)
    return auto_spect

def intspectrum(signal_freq1,signal_freq2) :
#signal_freq= tableaux contenant les FFTs fois le nombre de colonne
    inter_spect=numpy.zeros((numpy.shape(signal_freq1)[0]),dtype=complex)
    if  numpy.shape(signal_freq1)==numpy.shape(signal_freq2):
#        inter_spect=numpy.zeros((numpy.shape(signal_freq1)[0]),dtype=complex)
        inter_spect=numpy.conj(signal_freq1)*signal_freq2
    return inter_spect

def FFR(signal_freq1,signal_freq2):
   FFR_sortie=numpy.zeros((numpy.shape(signal_freq1)[0],3),dtype=complex)
   if  numpy.shape(signal_freq1)==numpy.shape(signal_freq2):
       FFR_sortie[:,0]=numpy.divide(intspectrum(signal_freq1,signal_freq2),autospectrum(signal_freq1))
       FFR_sortie[:,1]=numpy.divide(autospectrum(signal_freq2),intspectrum(signal_freq2,signal_freq1))
       FFR_sortie[:,2]=(FFR_sortie[:,0]+FFR_sortie[:,1])/2
   return FFR_sortie

def coherence(signal_freq1,signal_freq2):
   cohe=numpy.zeros((numpy.shape(signal_freq1)[0]),dtype=float)
   if  numpy.shape(signal_freq1)==numpy.shape(signal_freq2):
       cohe=numpy.zeros((numpy.shape(signal_freq1)[0]),dtype=float)
       cohe=numpy.absolute(intspectrum(signal_freq1,signal_freq2),dtype=float)/(numpy.sqrt(autospectrum(signal_freq1))*numpy.sqrt(autospectrum(signal_freq2)))
   return abs(cohe)


def define_scope_index(time_data,time_range):
    time_range=time_range/float(1000)
    if time_range>time_data[-1]:
        index=numpy.arange(numpy.size(time_data))
    else:
        index=[]
        midtime=time_data[int(numpy.size(time_data)/float(2))]
        for ii,ee in enumerate(time_data):
            if numpy.abs(ee-midtime)<0.5*time_range:
                    index.append(ii)
    return index


class analyseur(QtGui.QMainWindow, crosstone_gui.Ui_MAIN):
    def __init__(self, parent=None):
        super(analyseur, self).__init__(parent)
        self.setupUi(self)
        ############################################################
        self.TAB.setCurrentIndex(0)
        self.define_delta_f()
        self.set_input1_label()
        self.get_all_variable()
        self.actu_time_range()
        #################################
        self.autospectre=1e-7*numpy.ones((numpy.size(self.variable.data_freq),2))
        self.FRF12=1e-7*numpy.ones((numpy.size(self.variable.data_freq),2),dtype=complex)
        self.FRF21=1e-7*numpy.ones((numpy.size(self.variable.data_freq),2),dtype=complex)
        self.coherence=1e-7*numpy.ones(numpy.size(self.variable.data_freq))
        #################################
        self.FRF_MP_box.setChecked(True)
        self.save_button.setEnabled(False)
        self.set_window()
        self.monitorL=plot_monitorL(self.plot_temporel1,self.variable.input1_parameter[1])
        self.monitorR=plot_monitorR(self.plot_temporel2)
        self.auto=plot_autospectrum(self.plot_auto,'Frequency (Hz)','Amplitude (Units)')
        self.cohe=plotco(self.plot_coherence,'Frequency (Hz)','Amplitude (%)')
        self.frfa=plot2D(self.plot_FRF1,'Frequency (Hz)','Amplitude')
        self.frfb=plot2D(self.plot_FRF2,'Frequency (Hz)','Amplitude')
        self.FRF_db_box.setChecked(True)
        self.AUTO_dB_box.setChecked(True)
        ####################################"
        self.connectActions()


    def connectActions(self):
        self.TAB.currentChanged.connect(lambda: self.tab_changed())
        self.test_button.clicked.connect(lambda: self.test_main())
        self.stop_test_button.clicked.connect(lambda: self.stop_test())
        self.start_button.clicked.connect(lambda:self.measureFRF())
        self.stop_button.clicked.connect(lambda:self.stop_mesure())
        self.input1_cb.activated.connect(lambda: self.set_input1_label())
        self.input1_A_le.editingFinished.connect(lambda: self.input_update())
        self.input1_B_le.editingFinished.connect(lambda: self.input_update())
        self.input1_C_le.editingFinished.connect(lambda: self.stop_test())
        self.covering_le.editingFinished.connect(lambda: self.cover_update())
        self.covering2_le.editingFinished.connect(lambda: self.cover_update())
        self.windowed_cb.activated.connect(lambda: self.set_window())
        self.buffer_cb.activated.connect(lambda: self.define_delta_f())
        self.frequency_le.editingFinished.connect(lambda: self.define_delta_f())
        self.FRF_ref_le.editingFinished.connect(lambda: self.define_ref())
        self.FRF_MP_box.clicked.connect(lambda: self.RIchange())
        self.FRF_RI_box.clicked.connect(lambda: self.MPchange())
        self.FRF_box.activated.connect(lambda: self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq))
        self.FRF_db_box.clicked.connect(lambda: self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq))
        self.AUTO_dB_box.clicked.connect(lambda: self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq))
        self.save_button.clicked.connect(lambda: self.save_FRF())
        ####################" modification des infinite line des courbes
        self.frfa.hLine.sigDragged.connect(lambda: self.frfa.update_cross())
        self.frfa.vLine.sigDragged.connect(lambda: self.frfa.update_cross())
        self.frfb.hLine.sigDragged.connect(lambda: self.frfb.update_cross())
        self.frfb.vLine.sigDragged.connect(lambda: self.frfb.update_cross())
        self.auto.hLine.sigDragged.connect(lambda: self.auto.update_cross())
        self.auto.vLine.sigDragged.connect(lambda: self.auto.update_cross())
        self.monitorL.trigg.sigDragged.connect(lambda: self.trigger_update())
        #######################################################
        self.time_range_cb.activated.connect(lambda: self.actu_time_range())

    def tab_changed(self):
        self.stop_mesure()
        self.stop_test()

    def actu_time_range(self):
        self.variable.time_range=int(self.time_range_cb.currentText())
        time_sig=numpy.arange(self.variable.ratio*self.variable.buffer_size)/float(int(self.variable.ratio*self.variable.freq_max))
        self.index_signal=define_scope_index(time_sig,self.variable.time_range)


    def input_update(self):
            self.stop_test()
            if self.variable.input1_type=='Impulse':
                temp=self.input1_B_le.text() #covering for windowed
                if not temp:
                    self.input1_B_le.clear()
                    self.input1_B_le.insert('0.1')
                self.variable.input1_parameter[1]=float(self.input1_B_le.text())
                temp=self.input1_A_le.text()
                if not temp:
                    self.input1_A_le.clear()
                    self.input1_A_le.insert('5')
                self.variable.input1_parameter[0]=float(self.input1_A_le.text())
                if self.STOP_test==1:
                    self.courbe_trigg_update()

    def cover_update(self):
            self.stop_test()
            if self.variable.input1_type=='Impulse':
                temp=self.covering_le.text() #covering for windowed
                if not temp:
                    self.covering_le.clear()
                    self.covering_le.insert('25')
                self.variable.covering=float(self.covering_le.text())/100
                temp=self.covering2_le.text()
                if not temp:
                    self.covering2_le.clear()
                    self.covering2_le.insert('10')
                if self.variable.covering2>0.5:
                    self.covering2_le.clear()
                    self.covering2_le.insert('50')
                self.variable.covering2=float(self.covering2_le.text())/100 #covering for windowed
                if self.STOP_test==1:
                    self.courbe_trigg_update()

    def trigger_update(self):
           if self.variable.input1_type=='Impulse':
               y_min=0
               y_max=0.98*(self.monitorL.p.getViewBox().viewRange()[1][1])
               y=self.monitorL.trigg.value()
               ind_y=y
               if ind_y<y_min:
                   ind_y=0.01
                   self.monitorL.trigg.setValue(ind_y)
               if ind_y>y_max:
                   ind_y=y_max
                   self.monitorL.trigg.setValue(ind_y)
               self.variable.input1_parameter[1]=self.monitorL.trigg.value()
               self.input1_B_le.clear()
               self.input1_B_le.insert("{0:.2f}".format(self.variable.input1_parameter[1]))
            #met a jour les courbes et les enveloppe
               if self.STOP_test==1:
                   self.courbe_trigg_update()

    def courbe_trigg_update(self):
        time2=numpy.arange(2*self.BUFFER)/float(int(self.variable.ratio*self.variable.freq_max))
        poids_expo=numpy.zeros((2*self.BUFFER))
        poids_force=numpy.zeros((2*self.BUFFER))
        pretrigg=int(float(self.variable.input1_parameter[0]*self.BUFFER)/100)
        indice=numpy.array(numpy.where(self.signal_tempL.data>self.variable.input1_parameter[1])).astype('int')[0]
        if numpy.size(indice)>0 :
            poids_expo[indice[0]-pretrigg:indice[0]-pretrigg+self.BUFFER]=numpy.exp(-numpy.arange(self.BUFFER)/float(self.variable.covering*self.BUFFER))*max(abs(self.signal_tempR.data))
            ##################♣ calcul poids force window
            n_max=2*int(self.BUFFER*self.variable.covering2)
            for ii in range(0,2*int(self.BUFFER*self.variable.covering2)):# forvce window returns to 0 after 2*force time constant
                 if ii<int(self.BUFFER*self.variable.covering2):
                     poids_force[indice[0]-pretrigg+ii]=1*max(abs(self.signal_tempL.data))
                 else:
                     poids_force[indice[0]-pretrigg+ii]=0.5*(1+numpy.cos(numpy.pi*(ii+0.5*n_max)/((1-0.5)*n_max)))*max(abs(self.signal_tempL.data))
                #♣0.5--> forvce window returns to 0 after 2*force time constant
        #                    if self.signal_tempL.data[indice[0]]<0:
        #                        self.monitorL.update_data_impulse(time2,-1*self.signal_tempL.data,poids_force)
        #                        self.monitorR.update_data_impulse(time2,-1*self.signal_tempR.data,poids_expo)
        #                    else:
            self.monitorL.update_data_impulse(time2,self.signal_tempL.data,poids_force)
            self.monitorR.update_data_impulse(time2,self.signal_tempR.data,poids_expo)
    
    def RIchange(self):
        if self.FRF_RI_box.isChecked()==True:
            self.FRF_RI_box.setChecked(False)
        else:
            self.FRF_RI_box.setChecked(True)
        self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq)

    def MPchange(self):
        if self.FRF_MP_box.isChecked()==True:
            self.FRF_MP_box.setChecked(False)
        else:
            self.FRF_MP_box.setChecked(True)
        self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq)

    def define_ref(self):
        if not self.FRF_ref_le.text():
            self.FRF_ref_le.clear()
            self.FRF_ref_le.insert('1')
        self.variable.ref_db=float(self.FRF_ref_le.text())
        self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq)

    def get_all_variable(self):
        self.variable=Variable()
        temp = self.frequency_le.text() #max frequency to study
        if not temp:
            self.frequency_le.clear()
            self.frequency_le.insert('4000')
            self.variable.freq_max=4000
        else:
            self.variable.freq_max=float(self.frequency_le.text())
        #######################################
        self.variable.buffer_size = float(self.buffer_cb.currentText()) # size of buffer
        self.variable.delta_freq=self.delta_f #
        self.variable.data_freq=numpy.arange(0,self.variable.freq_max,self.variable.delta_freq)
        self.variable.window=self.windowed_cb.currentText() # windowed type
        self.BUFFER=int(self.variable.ratio*self.variable.buffer_size)
        self.signal_tempL=signal_BUFF()
        self.signal_tempL.init(2*self.BUFFER)
        self.signal_tempR=signal_BUFF()
        self.signal_tempR.init(2*self.BUFFER)
        ##############################
        temp=self.covering_le.text() #covering for windowed
        if not temp:
            self.covering_le.clear()
            self.covering_le.insert('66')
            self.variable.covering=0.66
        else:
            self.variable.covering=float(self.covering_le.text())/100

        temp=self.covering2_le.text()
        if not temp:
            self.covering2_le.clear()
            self.covering2_le.insert('50')
            self.variable.covering2=0.5
        else:
            self.variable.covering2=float(self.covering2_le.text())/100 #covering for windowed
        ######## force window must be inferiori to 50%
        if self.variable.covering2>0.5:
            self.covering2_le.clear()
            self.covering2_le.insert('50')
            self.variable.covering2=0.5

        ###################################
        temp = self.channel1_sens_le.text()
        if not temp:
            self.channel1_sens_le.clear()
            self.channel1_sens_le.insert('1')
            self.variable.channel1_sensitivity = 1
        else:
            self.variable.channel1_sensitivity = float(self.channel1_sens_le.text())

        temp = self.channel2_sens_le.text()
        if not temp:
            self.channel2_sens_le.clear()
            self.channel2_sens_le.insert('1000')
            self.variable.channel2_sensitivity = 1000
        else:
            self.variable.channel2_sensitivity = float(self.channel2_sens_le.text())/1000

        ######################################c
        self.variable.input1_type=self.input1_cb.currentText()

        temp=self.input1_A_le.text()
        if not temp:
            self.input1_A_le.clear()
            self.input1_A_le.insert('0')
        temp=self.input1_B_le.text()
        if not temp:
            self.input1_B_le.clear()
            self.input1_B_le.insert('0')
        temp=self.input1_C_le.text()
        if not temp:
            self.input1_C_le.clear()
            self.input1_C_le.insert('0')
        self.variable.input1_parameter=[float(self.input1_A_le.text()),float(self.input1_B_le.text()),float(self.input1_C_le.text())]
        
        #####################################################
        temp=self.FRF_ref_le.text()
        if not temp:
            self.variable.ref_db=1
            self.FRF_ref_le.clear()
            self.FRF_ref_le.insert('1')
        else:
            self.variable.ref_db=float(self.FRF_ref_le.text())

        #########################################
#        self.variable.saved_directory=self.path_le.text()
#        self.variable.measure_name=self.name_le.text()
        temp=self.average_le.text()
        if not temp:
            self.variable.nb_mean=10
            self.average_le.clear()
            self.average_le.insert('10')
        else:
            self.variable.nb_mean=int(self.average_le.text())
        self.variable.time_range=int(self.time_range_cb.currentText())
        
        ############## verification et correction si champ vide ########## (à faire pour tous ou trouvé sytème ingénieux)
#        gamme=self.range_edit.text()
#        if not gamme:
#            self.variable.range=1
#            self.range_edit.clear()
#            self.range_edit.insert('1')
#        else:
#            self.range=int(self.range_edit.text())


    def define_delta_f(self):
        self.STOP_test=1
        self.test_button.setEnabled(True)
        try:
            self.monitorL.reset(self.variable.input1_parameter[1])#nettoie et defini le plot monitor
            self.monitorR.reset()#nettoie et defini le plot monitor
        except:
            pass
        if float(self.frequency_le.text())>=400 and float(self.frequency_le.text())<17000 :# min freq and max samplinf freq
            self.delta_f=float(self.frequency_le.text())/float(self.buffer_cb.currentText())
            self.delta_value.setText("{0:.3f}".format(self.delta_f))

        else:
            self.frequency_le.clear()
            self.frequency_le.insert('1000')
            self.delta_value.clear()
            self.delta_value.setText('Freq=[400Hz,17000Hz]')
        if float(self.frequency_le.text())>9000:
            buff =int(self.buffer_cb.currentText()) # size of buffer
            if buff<400:
                self.delta_value.clear()
                self.delta_value.setText('FMax>9000Hz-->FFT line>=400')
                self.buffer_cb.setCurrentIndex(2)

    def set_window(self):
        self.STOP_test=1
        self.test_button.setEnabled(True)
        try:
            self.monitorL.reset(self.variable.input1_parameter[1])#nettoie et defini le plot monitor
            self.monitorR.reset()#nettoie et defini le plot monitor
        except:
            pass
        window_type=self.windowed_cb.currentText()
        if window_type=='Uniform':
            self.covering_label.setText('Covering (%)')
            self.covering_le.clear()
            self.covering_le.insert('0')
            self.covering_label.setVisible(False)
            self.covering_le.setEnabled(False)
            self.covering2_label.setVisible(False)
            self.covering2_le.setEnabled(False)
            self.covering2_le.clear()
            self.covering2_le.insert('0')
        if window_type=='Hanning':
            self.covering_label.setText('Covering (%)')
            self.covering_le.setEnabled(True)
            self.covering_le.clear()
            self.covering_le.insert("{0:.1f}".format(66))
            self.covering2_label.setVisible(False)
            self.covering2_le.setEnabled(False)
            self.covering2_le.clear()
            self.covering2_le.insert('0')
        if window_type=='Force/Exponential':
            self.covering2_label.setVisible(True)
            self.covering_label.setVisible(True)
            self.covering2_le.setEnabled(True)
            self.covering_le.setEnabled(True)
            self.covering_label.setText('Exponential Time Constant (%)')
            self.covering_le.clear()
            self.covering_le.insert('25')
            self.covering2_le.clear()
            self.covering2_label.setText('Force Time Constant (1-50%)')
            self.covering2_le.insert('10')

    def set_input1_label(self):
        self.STOP_test=1
        self.test_button.setEnabled(True)
        try:
            self.monitorL.reset(self.variable.input1_parameter[1])#nettoie et defini le plot monitor
            self.monitorR.reset()#nettoie et defini le plot monitor
        except AttributeError:
            pass
        input_type=self.input1_cb.currentText()
        self.input1_A_le.clear()
        self.input1_B_le.clear()
        self.input1_C_le.clear()
        if input_type=='None':
            self.windowed_cb.setCurrentIndex(0)
            self.set_window()
            self.input1_A_le.insert('0')
            self.input1_B_le.insert('0')
            self.input1_C_le.insert('0')
            self.input1_A_label.setVisible(False)
            self.input1_B_label.setVisible(False)
            self.input1_C_label.setVisible(False)
            self.input1_A_le.setEnabled(False)
            self.input1_B_le.setEnabled(False)
            self.input1_C_le.setEnabled(False)
        if input_type=='White noise':
            self.windowed_cb.setCurrentIndex(0)
            self.set_window()
            self.input1_A_label.setVisible(True)
            self.input1_B_label.setVisible(True)
            self.input1_C_label.setVisible(True)
            self.input1_A_le.setEnabled(True)
            self.input1_B_le.setEnabled(True)
            self.input1_C_le.setEnabled(True)
            ###############################################
            self.input1_A_label.setText('Range (0 to 1)')
            self.input1_A_le.insert('1')
            self.input1_B_le.insert('0')
            self.input1_C_le.insert('0')
            self.input1_B_label.setVisible(False)
            self.input1_C_label.setVisible(False)
            self.input1_B_le.setEnabled(False)
            self.input1_C_le.setEnabled(False)
        if input_type=='Sinus':
            self.windowed_cb.setCurrentIndex(0)
            self.set_window()
            self.input1_A_label.setVisible(True)
            self.input1_B_label.setVisible(True)
            self.input1_C_label.setVisible(True)
            self.input1_A_le.setEnabled(True)
            self.input1_B_le.setEnabled(True)
            self.input1_C_le.setEnabled(True)
            ################################
            self.input1_A_label.setText('Frequency (Hz)')
            self.input1_A_le.insert('1000')
            self.input1_B_label.setText('Range (0 to 1)')
            self.input1_B_le.insert('1')
            self.input1_C_label.setText('Phase (degree)')
            self.input1_C_le.insert('0')
        if input_type=='Impulse':
            # Force/exponential windiwo automaticcaly
            self.windowed_cb.setCurrentIndex(2)
            self.set_window()
            #######################################
            self.input1_A_label.setVisible(True)
            self.input1_B_label.setVisible(True)
            self.input1_C_label.setVisible(False)
            self.input1_A_le.setEnabled(True)
            self.input1_B_le.setEnabled(True)
            self.input1_C_le.setEnabled(False)
            ######################################
            self.input1_A_label.setText('Pre-Triggering (% of window)')
            self.input1_A_le.insert('5')
            self.input1_B_label.setText('Threshold (0 to 1)')
            self.input1_B_le.insert('0.1')
            self.input1_C_le.insert('0')

    def save_FRF(self):
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','FRF.txt')
        fid=open(filename,'w')
        line='Amplitude vs Freq\n'
        fid.write(line)
        fid.write("{:^8}".format('Freq')+';'+"{:^16}".format('PSD_1(V^2/Hz)')+';'+"{:^16}".format('PSD_2 (V^2/Hz)')+';'+"{:^24}".format('FRF1/2-H1(Units/Hz)')+';'+"{:^24}".format('FRF2/1-H1(Units/Hz)')+';'+"{:^24}".format('FRF1/2-H2(Units/Hz)')+';'+"{:^24}".format('FRF2/1-H2(Units/Hz)')+'\n')
        freq=numpy.arange(0,self.variable.freq_max,self.variable.delta_freq)
        for ii in range(0,numpy.size(freq)):
            fid.write("{:8.2f}".format(freq[ii])+';'+"{:16.4E}".format(self.autospectre[ii,0])+';'+"{:16.4E}".format(self.autospectre[ii,1])+';'+"{:24.4E}".format(self.FRF12[ii,0])+';'+"{:24.4E}".format(self.FRF21[ii,0])+';'+"{:24.4E}".format(self.FRF12[ii,1])+';'+"{:24.4E}".format(self.FRF21[ii,1])+'\n')
        fid.close()


    def plot_FRF(self,autospectre,FRF12,FRF21,coherence,freq):
        if self.AUTO_dB_box.isChecked()==True:
            self.auto.update_data_dB(freq,autospectre[:,0],autospectre[:,1],self.variable.ref_db,'Frequency (Hz)', 'Amplitude (dB)')#reference de comparaison au carré aussi
        else:
            self.auto.update_data(freq,autospectre[:,0],autospectre[:,1],'Frequency (Hz)', 'Amplitude (units)')

        self.cohe.update_data(freq,100*self.coherence)
        if self.FRF_box.currentIndex()==0:
            data=FRF12[:,0]
        if self.FRF_box.currentIndex()==1:
            data=FRF21[:,0]
        if self.FRF_box.currentIndex()==2:
            data=FRF12[:,1]
        if self.FRF_box.currentIndex()==3:
            data=FRF21[:,1]
        if self.FRF_MP_box.isChecked()==True:
            if self.FRF_db_box.isChecked()==True:
                self.frfa.update_data_dB(freq,numpy.absolute(data),self.variable.ref_db,'Frequency (Hz)', 'Magnitude (dB)')
                self.frfb.update_data(freq,numpy.angle(data),'Frequency (Hz)', 'Phase (rad)')
            else:
                self.frfa.update_data(freq,numpy.absolute(data),'Frequency (Hz)', 'Magnitude (units)')
                self.frfb.update_data(freq,numpy.angle(data),'Frequency (Hz)', 'Phase (rad)')
        elif self.FRF_RI_box.isChecked()==True:
            if self.FRF_db_box.isChecked()==True:
                self.frfa.update_data_dB(freq,numpy.real(data),self.variable.ref_db,'Frequency (Hz)', 'Real (dB)')
                self.frfb.update_data_dB(freq,numpy.imag(data),self.variable.ref_db,'Frequency (Hz)', 'Imag (dB)')
            else:
                self.frfa.update_data(freq,numpy.real(data),'Frequency (Hz)', 'Real (units)')
                self.frfb.update_data(freq,numpy.imag(data),'Frequency (Hz)', 'Imag (units)')

    def stop_test(self):
        self.STOP_test=1
        self.test_button.setEnabled(True)
        self.stop_test_button.setEnabled(False)
        ####### update lot si on est dans le mode 'impulse'
#        if self.variable.input1_type=='Impulse':
#            self.get_all_variable()
#            self.courbe_trigg_update()

    def stop_mesure(self):
        self.STOP_mesure=1
        self.start_button.setEnabled(True)
        self.stop_button.setEnabled(False)


    def read_callback(self,in_data, frame_count, time_info, status):
        if frame_count==self.BUFFER:
            self.signal_temp=numpy.reshape(numpy.fromstring(in_data, 'float32'),(int(frame_count),2))
#            print("READ CALLBACK\n")
        return (in_data, pyaudio.paContinue)

    def write_callback(self,out_data, frame_count, time_info, status):
        out_data=self.signal_input.astype(numpy.float32).tostring()
#        print("WRITE CALLBACK\n")
        return (out_data, pyaudio.paContinue)

    def test_main(self):
        self.get_all_variable()
        self.actu_time_range()#recupere les indices a plotter
        self.test_button.setEnabled(False)
        self.stop_test_button.setEnabled(True)
        #####################
        self.monitorL.reset(self.variable.input1_parameter[1])
        self.monitorR.reset()
        #####################
        RATE=int(self.variable.ratio*self.variable.freq_max)
        self.BUFFER=int(self.variable.ratio*self.variable.buffer_size)
        if self.variable.input1_type=='Impulse':
            buff=self.BUFFER
            time2=numpy.arange(2*self.BUFFER)/float(RATE)
            pretrigg=int(float(self.variable.input1_parameter[0]*buff)/100)
            poids_expo=numpy.zeros((2*self.BUFFER))
            poids_force=numpy.zeros((2*self.BUFFER))
#        self.BUFFER=int(self.variable.ratio*self.variable.buffer_size)
        time=numpy.arange(self.BUFFER)/float(RATE)
#        time=(time-(0.5*time[-1]))# ligne temporelles signal tempo (plot symmétrique)
        self.signal_input=numpy.zeros((self.BUFFER,2))
        self.signal_temp=numpy.zeros((self.BUFFER,2))
        ############### defini les buffer ###################
        self.signal_tempL=signal_BUFF()
        self.signal_tempL.init(2*self.BUFFER)
        self.signal_tempR=signal_BUFF()
        self.signal_tempR.init(2*self.BUFFER)
        
############################################################################
#        declarations des treams
############################################################################
        signal_test=pyaudio.PyAudio()
        stream_read = signal_test.open(format=pyaudio.paFloat32,
                                       channels=2,
                                       rate=RATE,
                                       input=True,
#                                      output=True,
                                       frames_per_buffer=self.BUFFER,
                                       stream_callback=self.read_callback)
        if self.variable.input1_type=='White noise' or self.variable.input1_type=='Sinus':
            stream_write = signal_test.open(format=pyaudio.paFloat32,
                                            channels=2,
                                            rate=RATE,
#                                           input=True,
                                            output=True,
                                            frames_per_buffer=self.BUFFER,
                                            stream_callback=self.write_callback)

        ####################### initialise signal ######################
        if self.variable.input1_type=='White noise':
            nb_mean=int(round(5/float(time[-1])))# nombre de période pour 10s
            sig_in=self.variable.input1_parameter[0]*numpy.random.uniform(-1,1,nb_mean*self.BUFFER)
            sig_in=numpy.reshape(sig_in,(nb_mean,self.BUFFER))
            end=0
        if self.variable.input1_type=='Sinus':
            nb_mean=int(round(5/float(time[-1])))
            tempo=numpy.arange(nb_mean*self.BUFFER)/float(RATE)
            sig_in=self.variable.input1_parameter[1]*(numpy.sin(2*numpy.pi*self.variable.input1_parameter[0]*tempo+(self.variable.input1_parameter[2]*numpy.pi/float(180))))
            sig_in=numpy.reshape(sig_in,(nb_mean,self.BUFFER))
            end=0

        self.STOP_test=0
        while self.STOP_test==0:
            stream_read.start_stream()
            try:
                stream_write.start_stream()
            except UnboundLocalError:
                pass
            
            ##################### input signals ####################
            if self.variable.input1_type=='White noise':
                self.signal_input[:,0]=sig_in[end,:]
                end=end+1
                if end==nb_mean:
                    end=0
            if self.variable.input1_type=='Sinus':
                self.signal_input[:,0]=sig_in[end,:]
                end=end+1
                if end==nb_mean:
                    end=0
            ############################################################
            temps.sleep(0.5*time[-1])#mets en pause le temps d'enregistrement d'un buffer
#            self.signal_tempL.add((1/float(self.variable.channel1_sensitivity))*self.range*self.signal_temp[:,0])
#            self.signal_tempR.add((1/float(self.variable.channel2_sensitivity))*self.range*self.signal_temp[:,1])
            self.signal_tempL.add((1/float(self.variable.channel1_sensitivity))*self.signal_temp[:,0])
            self.signal_tempR.add((1/float(self.variable.channel2_sensitivity))*self.signal_temp[:,1])
################################################################################################################
            if self.variable.input1_type=='Impulse':
                self.monitorL.update_data_impulse(time2,self.signal_tempL.data,poids_force)
                self.monitorR.update_data_impulse(time2,self.signal_tempR.data,poids_expo)
                
                ###########################################"
                # detection de l'impulsion et verification que le signal est bon
                ###################################################
                indice=[]
                indice=numpy.array(numpy.where(self.signal_tempL.data>self.variable.input1_parameter[1])).astype('int')[0]
                if numpy.size(indice)>0 and (indice[0]-pretrigg)>0 and (indice[0]-pretrigg+buff)<2*self.BUFFER:
                    poids_expo[indice[0]-pretrigg:indice[0]-pretrigg+buff]=numpy.exp(-numpy.arange(buff)/float(self.variable.covering*buff))*max(abs(self.signal_tempR.data))
                    ##################♣ calcul poids force window
                    n_max=2*int(buff*self.variable.covering2)
                    for ii in range(0,2*int(buff*self.variable.covering2)):# forvce window returns to 0 after 2*force time constant
                        if ii<int(buff*self.variable.covering2):
                            poids_force[indice[0]-pretrigg+ii]=1*max(abs(self.signal_tempL.data))
                        else:
                          poids_force[indice[0]-pretrigg+ii]=0.5*(1+numpy.cos(numpy.pi*(ii+0.5*n_max)/((1-0.5)*n_max)))*max(abs(self.signal_tempL.data))
                          #♣0.5--> forvce window returns to 0 after 2*force time constant
#                    if self.signal_tempL.data[indice[0]]<0:
#                        self.monitorL.update_data_impulse(time2,-1*self.signal_tempL.data,poids_force)
#                        self.monitorR.update_data_impulse(time2,-1*self.signal_tempR.data,poids_expo)
#                    else:
                    self.monitorL.update_data_impulse(time2,self.signal_tempL.data,poids_force)
                    self.monitorR.update_data_impulse(time2,self.signal_tempR.data,poids_expo)
                    self.stop_test()
            else:
            ################# plot recorded signals #############################
                self.monitorL.update_data(time[self.index_signal],self.signal_tempL.data[self.index_signal])
                self.monitorR.update_data(time[self.index_signal],self.signal_tempR.data[self.index_signal])
            QtGui.QApplication.processEvents()
            #####################################################################
        stream_read.stop_stream()
        stream_read.close()
        try:
            stream_write.stop_stream()
            stream_write.close
        except:
            pass
        signal_test.terminate()

    def measureFRF(self):
        self.stop_test()
        self.get_all_variable()
        self.start_button.setEnabled(False)
        self.stop_button.setEnabled(True)
        self.save_button.setEnabled(False)
        QtGui.QApplication.processEvents()
        self.frfa.reset()#remet les lignes hori et verti à 0
        self.frfb.reset()
        self.auto.reset()
        ####☻ init variable #########
        freq=self.variable.data_freq# grille fréquentielle
        self.autospectre=1e-7*numpy.ones((numpy.size(freq),2))
        self.FRF12=1e-7*numpy.ones((numpy.size(freq),3),dtype=complex)
        self.FRF21=1e-7*numpy.ones((numpy.size(freq),3),dtype=complex)
        self.coherence=1e-7*numpy.ones(numpy.size(freq))
        ##########################################
        # initialisation
        ##########################################
        RATE=int(self.variable.ratio*self.variable.freq_max)
        self.BUFFER=int(self.variable.ratio*self.variable.buffer_size)
        if self.variable.input1_type=='Impulse':
            #si marteau la taille du buffer est doublé pour avoir assez d epoint pour prendre un buffer normale complet
            buff=self.BUFFER
            pretrigg=int(float(self.variable.input1_parameter[0]*buff)/100)
            poids_expo=numpy.zeros((buff))
            poids_expo=numpy.exp(-numpy.arange(buff)/float(self.variable.covering*buff))
            poids_force=numpy.zeros((buff))
            n_max=2*int(buff*self.variable.covering2)
            for ii in range(0,2*int(buff*self.variable.covering2)):# forvce window returns to 0 after 2*force time constant
                if ii<int(buff*self.variable.covering2):
                    poids_force[ii]=1
                else:
                     poids_force[ii]=0.5*(1+numpy.cos(numpy.pi*(ii+0.5*n_max)/((1-0.5)*n_max)))
        freq=numpy.arange(0,self.variable.freq_max,self.variable.delta_freq)
        self.signal_input=numpy.zeros((self.BUFFER,2))
        self.signal_temp=numpy.zeros((self.BUFFER,2))
        signal_tempL=signal_BUFF()
        signal_tempL.init(2*self.BUFFER)
        signal_tempR=signal_BUFF()
        signal_tempR.init(2*self.BUFFER)
        time=self.BUFFER/float(RATE)# temps d'enregistrement d'un buffer
        data_fftL=numpy.zeros(self.BUFFER,dtype=complex)
        data_fftR=numpy.zeros(self.BUFFER,dtype=complex)
        self.FFT_out=numpy.zeros((self.BUFFER,2),dtype=complex)
        ############################################################################
        #        declaration des streams
        ############################################################################
        signal_test=pyaudio.PyAudio()
        stream_read = signal_test.open(format=pyaudio.paFloat32,
                                       channels= 2,
                                       rate=RATE,
                                       input=True,
#                                      output=True,
                                       frames_per_buffer=self.BUFFER,
                                       stream_callback=self.read_callback)
        if self.variable.input1_type=='White noise' or self.variable.input1_type=='Sinus':
            stream_write = signal_test.open(format=pyaudio.paFloat32,
                                            channels= 2,
                                            rate=RATE,
#                                           input=True,
                                            output=True,
                                            frames_per_buffer=self.BUFFER,
                                            stream_callback=self.write_callback)
        #################################################################################
        ########### definition poids fenetre, du nombre de recouvrement e des indices dans le buffer ############
        if self.variable.window=='Uniform' :
            poids=numpy.ones((self.BUFFER))
        if self.variable.window=='Hanning' :
            poids=0.5-0.5*(numpy.cos(2*numpy.pi*numpy.arange(0,self.BUFFER)/self.BUFFER))
            ###### indice consitituant les différents recouvrement #######
        taux_overlap=self.variable.covering
        nb_cover=int(round(1/(1-taux_overlap)))
        ind=numpy.zeros((self.BUFFER,nb_cover),dtype=int)
        for j in range(0,nb_cover):
            ind[:,j]= numpy.arange(j*(1-taux_overlap)*self.BUFFER,j*(1-taux_overlap)*self.BUFFER+self.BUFFER).astype(int)
        self.nb_make.display('0')
        
        self.STOP_mesure=0
        while self.STOP_mesure==0:
            stream_read.start_stream()
            try:
                stream_write.start_stream()
            except UnboundLocalError:
                pass

            ii=0
            kk=0
            #######generation du signal sinus et decoupage en nombre de moyenne
            if self.variable.input1_type=='White noise':
#                sig_in=self.variable.input1_parameter[0]*numpy.random.uniform(-1,1,self.variable.nb_mean*self.BUFFER)
                sig_in=self.variable.input1_parameter[0]*numpy.random.uniform(-1,1,self.variable.nb_mean*self.BUFFER)
                sig_in=numpy.reshape(sig_in,(self.variable.nb_mean,self.BUFFER))
            if self.variable.input1_type=='Sinus':
                tempo=numpy.arange(self.variable.nb_mean*self.BUFFER)/float(RATE)
                sig_in=self.variable.input1_parameter[1]*(numpy.sin(2*numpy.pi*self.variable.input1_parameter[0]*tempo+(self.variable.input1_parameter[2]*numpy.pi/float(180))))
                sig_in=numpy.reshape(sig_in,(self.variable.nb_mean,self.BUFFER))
            ####################################################
            while ii<self.variable.nb_mean and self.STOP_mesure==0:
                ##################### input signals ####################
                if self.variable.input1_type=='White noise':
#                    self.signal_input[:,0]=self.variable.input1_parameter[0]*numpy.random.uniform(-1,1,self.BUFFER)
                    self.signal_input[:,0]=sig_in[ii,:]
                if self.variable.input1_type=='Sinus':
                    self.signal_input[:,0]=sig_in[ii,:]
#                    self.signal_input[:,0]=self.variable.input1_parameter[1]*(numpy.sin(2*numpy.pi*self.variable.input1_parameter[0]*time+(self.variable.input1_parameter[2]*numpy.pi/float(180))))
                ########################################################
                if self.variable.input1_type=='Impulse':
                    QtGui.QApplication.processEvents()
                    temps.sleep(0.5*time) # Mets en pause le temps d'enregistrement d'un buffer
#                    signal_tempL.add((1/float(self.variable.channel1_sensitivity))*self.range*self.signal_temp[:,0])
#                    signal_tempR.add((1/float(self.variable.channel2_sensitivity))*self.range*self.signal_temp[:,1])
                    signal_tempL.add((1/float(self.variable.channel1_sensitivity))*self.signal_temp[:,0])
                    signal_tempR.add((1/float(self.variable.channel2_sensitivity))*self.signal_temp[:,1])
                    indice=numpy.array(numpy.where(signal_tempL.data>self.variable.input1_parameter[1])).astype('int')[0]
                    if numpy.size(indice)>0 and (indice[0]-pretrigg)>0 and (indice[0]-pretrigg+buff)<2*self.BUFFER:
                        ii=ii+1
                        data_fftL=2*numpy.fft.fft(fenetre(numpy.multiply(poids_force,poids_expo),signal_tempL.data[indice[0]-pretrigg:indice[0]-pretrigg+buff]),buff)/float(buff)
                        data_fftR=2*numpy.fft.fft(fenetre(poids_expo,signal_tempR.data[indice[0]-pretrigg:indice[0]-pretrigg+buff]),buff)/float(buff)
                        data_fftL[0]=0.5*data_fftL[0]#la composante continu n'est pas multipliee par 2
                        data_fftR[0]=0.5*data_fftR[0]#la composante continu n'est pas multipliee par 2
                        print(data_fftL)
                        kk += 1
                        self.autospectre[:,0]=((kk-1)/float(kk))*self.autospectre[:,0]+(autospectrum(data_fftL[0:numpy.size(freq)])/float(self.variable.delta_freq))/float(kk)#calcul en PSD--> divise par freq echan + moyennage
                        self.autospectre[:,1]=((kk-1)/float(kk))*self.autospectre[:,1]+(autospectrum(data_fftR[0:numpy.size(freq)])/float(self.variable.delta_freq))/float(kk)#calcul en PSD
                        self.coherence=((kk-1)/float(kk))*self.coherence+(coherence(data_fftL[0:numpy.size(freq)],data_fftR[0:numpy.size(freq)]))/float(kk)
                        self.FRF12=((kk-1)/float(kk))*self.FRF12+(FFR(data_fftL[0:numpy.size(freq)],data_fftR[0:numpy.size(freq)]))/float(kk)
                        self.FRF21=((kk-1)/float(kk))*self.FRF21+(FFR(data_fftR[0:numpy.size(freq)],data_fftL[0:numpy.size(freq)]))/float(kk)
                        
#                        self.coherence=coherence(self.FFT_out[:,0],self.FFT_out[:,1])
#                        for jj in range(len(self.FFT_out)):
#                            self.FFT_out[jj,0]=((ii-1)/float(ii))*self.FFT_out[jj,0]+(data_fftL[jj])/float(ii)
#                            self.FFT_out[jj,1]=((ii-1)/float(ii))*self.FFT_out[jj,1]+(data_fftR[jj])/float(ii)
#                            self.autospectre[jj,0]=autospectrum(self.FFT_out[jj,0])/float(self.variable.delta_freq)#calcul en PSD--> divise par freq echan
#                            self.autospectre[jj,1]=autospectrum(self.FFT_out[jj,1])/float(self.variable.delta_freq)#calcul en PSD
#                            self.FRF12=FFR(self.FFT_out[jj,0],self.FFT_out[jj,1])
#                            self.FRF21=FFR(self.FFT_out[jj,1],self.FFT_out[jj,0])
#                            self.coherence=coherence(self.FFT_out[jj,0],self.FFT_out[jj,1])
                        self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,freq)
                        #####################♪ remet les buffer à zeros
                        signal_tempL.reset()
                        signal_tempR.reset()
                        self.signal_temp=numpy.zeros((self.BUFFER,2))
                        indice=[]
                        temps.sleep(1)
                        self.nb_make.display(int(ii))
#                        if self.STOP_mesure==1:
#                            ii=self.variable.nb_mean+1
#                            self.nb_make.display(0)
                        #################################################
                    
#                    self.nb_make.display(int(ii))
#                        if self.STOP_mesure==1:
#                            ii=self.variable.nb_mean+1
#                            self.nb_make.display(0)
        
                else:
                    ii=ii+1
#                    for jj in range(0,nb_cover):
#                        kk=kk+1
                    self.ind=ind
                    temps.sleep(0.01)#mets en pause le temps d'enregistrement d'un buffer
                        
#                        signal_tempL.add((1/float(self.variable.channel1_sensitivity))*self.range*self.signal_temp[:,0])
#                        signal_tempR.add((1/float(self.variable.channel2_sensitivity))*self.range*self.signal_temp[:,1])
                    signal_tempL.add((1/float(self.variable.channel1_sensitivity))*self.signal_temp[:,0])
                    signal_tempR.add((1/float(self.variable.channel2_sensitivity))*self.signal_temp[:,1])
                    for jj in range(0,nb_cover):
                        kk=kk+1
#                        ####### calcul des FFT, autospectre, FRF, et coherence
                        data_fftL=numpy.zeros((self.BUFFER),dtype=complex)
                        data_fftR=numpy.zeros((self.BUFFER),dtype=complex)
                        data_fftL=2*numpy.fft.fft(fenetre(poids,signal_tempL.data[ind[:,jj]]),self.BUFFER)/float(self.BUFFER)
                        data_fftR=2*numpy.fft.fft(fenetre(poids,signal_tempR.data[ind[:,jj]]),self.BUFFER)/float(self.BUFFER)
                        data_fftL[0]=0.5*data_fftL[0]#la composante continu n'est pas multipliee par 2
                        data_fftR[0]=0.5*data_fftR[0]#la composante continu n'est pas multipliee par 2
                        ##########################################################################
                        self.autospectre[:,0]=((kk-1)/float(kk))*self.autospectre[:,0]+(autospectrum(data_fftL[0:numpy.size(freq)])/float(self.variable.delta_freq))/float(kk)#calcul en PSD--> divise par freq echan + moyennage
                        self.autospectre[:,1]=((kk-1)/float(kk))*self.autospectre[:,1]+(autospectrum(data_fftR[0:numpy.size(freq)])/float(self.variable.delta_freq))/float(kk)#calcul en PSD
                        self.coherence=((kk-1)/float(kk))*self.coherence+(coherence(data_fftL[0:numpy.size(freq)],data_fftR[0:numpy.size(freq)]))/float(kk)
                        self.FRF12=((kk-1)/float(kk))*self.FRF12+(FFR(data_fftL[0:numpy.size(freq)],data_fftR[0:numpy.size(freq)]))/float(kk)
                        self.FRF21=((kk-1)/float(kk))*self.FRF21+(FFR(data_fftR[0:numpy.size(freq)],data_fftL[0:numpy.size(freq)]))/float(kk)
#                        self.coherence=coherence(self.FFT_out[:,0],self.FFT_out[:,1])
                    self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,freq)
                    QtGui.QApplication.processEvents()
                    self.nb_make.display(int(ii))
#                    if self.STOP_mesure==1:
#                        ii=self.variable.nb_mean+1
#                        self.nb_make.display(0)
            self.frfa.reset()#remet les lignes hori et verti à 0
            self.frfb.reset()
            self.auto.reset()
            self.stop_mesure()
            stream_read.stop_stream()
            stream_read.close()
            try:
                stream_write.stop_stream()
                stream_write.close
            except UnboundLocalError:
                pass
            signal_test.terminate()
            self.save_button.setEnabled(True)
            QtGui.QApplication.processEvents()

#gui.auto.update_rest()
if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    style = QtGui.QStyleFactory.create('Plastique')
    app.setStyle(style)
    splash=splashScreen()
    splash.show_splash()
    app.processEvents()
    QtCore.QThread.sleep(3)
    splash.close_splash()
    gui = analyseur()
    gui.show()
    app.exec_()
    me = os.getpid()
    kill_proc_tree(me)