# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 16:33:32 2015
Updated in Feb 2020 by Emmanuel Leguet
@author: Yorick
"""

#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2017 Yorick Buot de l'Epine

#
#CrossTone is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published by
# the Free Software Foundation.

###############################################################################
################################### Imports ###################################
###############################################################################

import sys
import os.path
import psutil

import numpy as np
import time as temps
import pyaudio

from PyQt5 import QtCore, QtGui
import crosstone_gui
from class_PLOT import plot_monitorL, plot_monitorR, plot_autospectrum, plotco, plot2D


###############################################################################
################################## Functions ##################################
###############################################################################

# To find the path to additional files after conversion to .exe with pyinstaller
def resource_path(relative):
    '''Get absolute path to resource, works for dev and for PyInstaller'''
    return os.path.join(os.environ.get("_MEIPASS2",os.path.abspath(".")),relative)


def kill_proc_tree(pid, including_parent=True):
    ''' Close the program'''
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    if including_parent:
        parent.kill()

def fenetre(poids,bloc):
    bloc_out = np.multiply(poids,bloc)
    return bloc_out


def autospectrum(signal_freq):
    auto_spect = np.abs(np.conj(signal_freq)*signal_freq)
    return auto_spect


def intspectrum(signal_freq1,signal_freq2):
    inter_spect = np.conj(signal_freq1)*signal_freq2
    return inter_spect


def define_scope_index(time_data,time_range):
    time_range = time_range/float(1000)
    if time_range > time_data[-1]:
        index = np.arange(np.size(time_data))
    else:
        index = []
        midtime = time_data[int(np.size(time_data)/float(2))]
        for ii,ee in enumerate(time_data):
            if np.abs(ee-midtime) < 0.5*time_range:
                    index.append(ii)
    return index


###############################################################################
################################### Classes ###################################
###############################################################################

###################### Opening picture of the software ########################
class splashScreen:
    
    def __init__(self):
        self.splash=QtGui.QWidget()
        splash_pix = QtGui.QPixmap(os.getcwd()+'\splash_crosstone.png')
        self.splash.setFixedSize(splash_pix.size())
        self.splash.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        palette = QtGui.QPalette()
        palette.setBrush(10, QtGui.QBrush(splash_pix)) # 10 = Windowrole
        self.splash.setPalette(palette)

    def show_splash(self):
        self.splash.show()
        
    def close_splash(self):
        self.splash.close()


############################ Initialize variables #############################
class Variable:
    
    def __init__(self):
        self.freq_max=[] # max frequency to study
        self.ratio=2.0
        self.buffer_size =[] # size of buffer
        self.delta_freq=[] #
        self.window=[] # windowed type
        self.covering=[] # covering for windowed hanning 
        self.exponential = [] # for exponential window
        self.covering2=[] # covering for windowed Force impact
        self.channel1_sensitivity=[]
        self.channel2_sensitivity=[]
        self.input1_type=[]
        self.input1_parameter=[]
        self.ref_db=[]
        self.time_range=[]
        self.nb_mean=[]
        self.data_freq=[]

    def clear(self):
        self.freq_max=[] # max frequency to study
        self.ratio=2.0
        self.buffer_size=[] # size of buffer
        self.delta_freq=[] #
        self.window=[] # windowed type
        self.covering=[] # covering for windowed hanning or exponential
        self.exponential = [] # for exponential window
        self.covering2=[] # covering for windowed Force impact
        self.channel1_sensitivity=[]
        self.channel2_sensitivity=[]
        self.input1_type=[]
        self.input1_parameter=[]
        self.ref_db=[]
        self.nb_mean=[]
        self.time_range=[]
        self.data_freq=[]


############################### Buffer class ##################################
class signal_BUFF:
    
    def init(self,signal_size):
        self.data = 1e-7*np.ones((signal_size)) # 1e-7 to replace 0 (prevent issue in dB), same in the rest of the code
        
    def add(self,x):
        self.data[0:np.size(x)] = x
        self.data = np.roll(self.data,-np.size(x))
        
    def reset(self):
        self.data = 1e-7*np.ones((np.size(self.data)))


 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # MAIN CLASS # # # # # # # # # # # # # # # # # #
 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
class analyseur(QtGui.QMainWindow, crosstone_gui.Ui_MAIN):
    
    def __init__(self, parent=None):
        super(analyseur, self).__init__(parent)
        self.setupUi(self)
        #####
        self.setWindowTitle("CrossTone")
#        self.setWindowIcon(QtGui.QIcon(resource_path('D:\\Users\\Utilisateur\\Desktop\\SOFT_CROSSTONE\\source\\logo_crosstone.ico')))
        self.setWindowIcon(QtGui.QIcon(os.getcwd()+'\logo_crosstone.ico'))
        #####
        self.TAB.setCurrentIndex(0)
        self.define_delta_f()
        self.set_input1_label()
        self.get_all_variable()
        self.actu_time_range()
        #####
        self.autospectre=1e-7*np.ones((np.size(self.variable.data_freq),2))
        self.FRF12=1e-7*np.ones((np.size(self.variable.data_freq),2),dtype=complex)
        self.FRF21=1e-7*np.ones((np.size(self.variable.data_freq),2),dtype=complex)
        self.coherence=1e-7*np.ones(np.size(self.variable.data_freq))
        #####
        self.FRF_MP_box.setChecked(True)
        self.save_button.setEnabled(False)
        self.set_window()
        self.monitorL=plot_monitorL(self.plot_temporel1,self.variable.input1_parameter[1])
        self.monitorR=plot_monitorR(self.plot_temporel2)
        self.auto=plot_autospectrum(self.plot_auto,'Frequency (Hz)','Amplitude (Units)')
        self.cohe=plotco(self.plot_coherence,'Frequency (Hz)','Amplitude (%)')
        self.frfa=plot2D(self.plot_FRF1,'Frequency (Hz)','Amplitude')
        self.frfb=plot2D(self.plot_FRF2,'Frequency (Hz)','Amplitude')
        self.FRF_db_box.setChecked(True)
        self.measure_reject = False
        #####
        self.FRF_ref_le.clear()
        self.FRF_ref_le.insert('1e-6')
        self.variable.ref_db=float(self.FRF_ref_le.text())
        self.average_le.clear()
        self.average_le.insert('10')
        self.variable.nb_mean = 10
        #####
        self.connectActions()        
        
        
    def closeEvent(self, event):
        ''' Called to shut down the program in infinite while loops, if the user close the window'''
        me = os.getpid()
        kill_proc_tree(me)


    def connectActions(self):
        ''' Action triggered by another action'''
        self.TAB.currentChanged.connect(lambda: self.tab_changed())
        self.test_button.clicked.connect(lambda: self.test_main())
        self.stop_test_button.clicked.connect(lambda: self.stop_test())
        self.start_button.clicked.connect(lambda:self.measureFRF())
        self.stop_button.clicked.connect(lambda:self.stop_mesure())
        self.reject_button.clicked.connect(lambda:self.reject_measure())
        self.input1_cb.activated.connect(lambda: self.set_input1_label())
        self.input1_A_le.editingFinished.connect(lambda: self.input_update())
        self.input1_B_le.editingFinished.connect(lambda: self.input_update())
        self.input1_C_le.editingFinished.connect(lambda: self.stop_test())
        self.covering_le.editingFinished.connect(lambda: self.cover_update())
        self.expo_le.editingFinished.connect(lambda: self.cover_update())
        self.covering2_le.editingFinished.connect(lambda: self.cover_update())
        self.windowed_cb.activated.connect(lambda: self.set_window())
        self.buffer_cb.activated.connect(lambda: self.define_delta_f())
        self.frequency_le.editingFinished.connect(lambda: self.define_delta_f())
        self.FRF_ref_le.editingFinished.connect(lambda: self.define_ref())
        self.FRF_MP_box.clicked.connect(lambda: self.RIchange())
        self.FRF_RI_box.clicked.connect(lambda: self.MPchange())
        self.FRF_box.activated.connect(lambda: self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq))
        self.FRF_db_box.clicked.connect(lambda: self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq))
        self.save_button.clicked.connect(lambda: self.save_FRF())
        self.time_range_cb.activated.connect(lambda: self.actu_time_range())
        
        ##### refresh the infinite / visualization lines #####
        self.frfa.hLine.sigDragged.connect(lambda: self.frfa.update_cross())
        self.frfa.vLine.sigDragged.connect(lambda: self.frfa.update_cross())
        self.frfb.hLine.sigDragged.connect(lambda: self.frfb.update_cross())
        self.frfb.vLine.sigDragged.connect(lambda: self.frfb.update_cross())
        self.auto.hLine.sigDragged.connect(lambda: self.auto.update_cross())
        self.auto.vLine.sigDragged.connect(lambda: self.auto.update_cross())
        self.monitorL.trigg.sigDragged.connect(lambda: self.trigger_update())
        


    def tab_changed(self):
        self.stop_mesure()
        self.stop_test()

    def actu_time_range(self):
        self.variable.time_range = int(self.time_range_cb.currentText())
        time_sig = np.arange(self.variable.ratio*self.variable.buffer_size)/float(int(self.variable.ratio*self.variable.freq_max))
        self.index_signal = define_scope_index(time_sig,self.variable.time_range)
 

    def input_update(self):
        self.stop_test()
        
        if self.variable.input1_type == 'Impulse':
            temp = self.input1_B_le.text() # covering for windowed
            if not temp:
                self.input1_B_le.clear()
                self.input1_B_le.insert('0.1')
            self.variable.input1_parameter[1]=float(self.input1_B_le.text())
            temp = self.input1_A_le.text()
            if not temp:
                self.input1_A_le.clear()
                self.input1_A_le.insert('5')
            self.variable.input1_parameter[0]=float(self.input1_A_le.text())
            if self.STOP_test==1:
                self.courbe_trigg_update()

    def cover_update(self):
        self.stop_test()
        
        if self.variable.input1_type == 'Impulse':
            temp=self.covering_le.text() # covering for windowed
            if not temp:
                self.covering_le.clear()
                self.covering_le.insert('0.66')
            self.variable.covering=float(self.covering_le.text())
            temp=self.expo_le.text() # covering for windowed
            if not temp:
                self.expo_le.clear()
                self.expo_le.insert('25')
            self.variable.exponential=float(self.expo_le.text())/100
            temp=self.covering2_le.text()
            if not temp:
                self.covering2_le.clear()
                self.covering2_le.insert('10')
            if self.variable.covering2>0.5:
                self.covering2_le.clear()
                self.covering2_le.insert('50')
            self.variable.covering2=float(self.covering2_le.text())/100 # covering for windowed
            if self.STOP_test==1:
                self.courbe_trigg_update()

    def trigger_update(self):
        if self.variable.input1_type == 'Impulse':
           y_min = 0
           y_max = 0.98*(self.monitorL.p.getViewBox().viewRange()[1][1])
           y = self.monitorL.trigg.value()
           ind_y = y
           if ind_y < y_min:
               ind_y = 0.01
               self.monitorL.trigg.setValue(ind_y)
           if ind_y > y_max:
               ind_y = y_max
               self.monitorL.trigg.setValue(ind_y)
           self.variable.input1_parameter[1]=self.monitorL.trigg.value()
           self.input1_B_le.clear()
           self.input1_B_le.insert("{0:.2f}".format(self.variable.input1_parameter[1]))
           ##### refresh curves and enveloppes #####
           if self.STOP_test==1:
               self.courbe_trigg_update()

    def courbe_trigg_update(self):
        time2 = np.arange(2*self.BUFFER)/float(int(self.variable.ratio*self.variable.freq_max))
        poids_expo = np.zeros((2*self.BUFFER))
        poids_force = np.zeros((2*self.BUFFER))
        pretrigg = int(float(self.variable.input1_parameter[0]*self.BUFFER)/100)
        indice = np.array(np.where(self.signal_tempL.data>self.variable.input1_parameter[1])).astype('int')[0]
        
        if np.size(indice) > 0 :
            poids_expo[indice[0]-pretrigg:indice[0]-pretrigg+self.BUFFER] = np.exp(-np.arange(self.BUFFER)/float(self.variable.exponential*self.BUFFER))*max(abs(self.signal_tempR.data))
            ##### calculation poids force window #####
            n_max = 2*int(self.BUFFER*self.variable.covering2)
            for ii in range(0,2*int(self.BUFFER*self.variable.covering2)): # forvce window returns to 0 after 2*force time constant
                 if ii < int(self.BUFFER*self.variable.covering2):
                     poids_force[indice[0]-pretrigg+ii] = 1*max(abs(self.signal_tempL.data))
                 else:
                     poids_force[indice[0]-pretrigg+ii]=0.5*(1+np.cos(np.pi*(ii+0.5*n_max)/((1-0.5)*n_max)))*max(abs(self.signal_tempL.data))
            self.monitorL.update_data_impulse(time2,self.signal_tempL.data,poids_force)
            self.monitorR.update_data_impulse(time2,self.signal_tempR.data,poids_expo)
    
    def RIchange(self):
        if self.FRF_RI_box.isChecked():
            self.FRF_RI_box.setChecked(False)
        else:
            self.FRF_RI_box.setChecked(True)
        self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq)

    def MPchange(self):
        if self.FRF_MP_box.isChecked():
            self.FRF_MP_box.setChecked(False)
        else:
            self.FRF_MP_box.setChecked(True)
        self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq)

    def define_ref(self):
        if not self.FRF_ref_le.text():
            self.FRF_ref_le.clear()
            self.FRF_ref_le.insert('1')
        self.variable.ref_db=float(self.FRF_ref_le.text())
        self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,self.variable.data_freq)

    def get_all_variable(self):
        self.variable=Variable()
        temp = self.frequency_le.text() # max frequency to study
        if not temp:
            self.frequency_le.clear()
            self.frequency_le.insert('4000')
            self.variable.freq_max = 4000
        else:
            self.variable.freq_max=float(self.frequency_le.text())
        #####
        self.variable.buffer_size = float(self.buffer_cb.currentText()) # size of buffer
        self.variable.delta_freq = self.delta_f
        self.variable.data_freq = np.arange(0,self.variable.freq_max,self.variable.delta_freq)
        self.variable.window = self.windowed_cb.currentText() # windowed type
        self.BUFFER = int(self.variable.ratio*self.variable.buffer_size)
        self.signal_tempL = signal_BUFF()
        self.signal_tempL.init(2*self.BUFFER)
        self.signal_tempR = signal_BUFF()
        self.signal_tempR.init(2*self.BUFFER)
        #####
        temp=self.covering_le.text() # covering for windowed
        if not temp:
            self.covering_le.clear()
            self.covering_le.insert('66')
            self.variable.covering = 0.66
        else:
            self.variable.covering=float(self.covering_le.text())/100

        temp=self.expo_le.text() # covering for windowed
        if not temp:
            self.expo_le.clear()
            self.expo_le.insert('25')
            self.variable.exponential = 25
        else:
            self.variable.exponential=float(self.expo_le.text())/100

        temp=self.covering2_le.text()
        if not temp:
            self.covering2_le.clear()
            self.covering2_le.insert('50')
            self.variable.covering2 = 0.5
        else:
            self.variable.covering2 = float(self.covering2_le.text())/100 # covering for windowed
        ##### force window must be inferiori to 50% #####
        if self.variable.covering2 > 0.5:
            self.covering2_le.clear()
            self.covering2_le.insert('50')
            self.variable.covering2 = 0.5

        #####
        temp = self.channel1_sens_le.text()
        if not temp:
            self.channel1_sens_le.clear()
            self.channel1_sens_le.insert('1000') # mV/Unit
            self.variable.channel1_sensitivity = 1000 # mV/Unit
        else:
            self.variable.channel1_sensitivity = float(self.channel1_sens_le.text())/1000 # V/Unit

        temp = self.channel2_sens_le.text()
        if not temp:
            self.channel2_sens_le.clear()
            self.channel2_sens_le.insert('1000') # mV/Unit
            self.variable.channel2_sensitivity = 1000 # mV/Unit
        else:
            self.variable.channel2_sensitivity = float(self.channel2_sens_le.text())/1000 # V/Unit

        #####
        self.variable.input1_type=self.input1_cb.currentText()

        temp=self.input1_A_le.text()
        if not temp:
            self.input1_A_le.clear()
            self.input1_A_le.insert('0')
        temp=self.input1_B_le.text()
        if not temp:
            self.input1_B_le.clear()
            self.input1_B_le.insert('0')
        temp=self.input1_C_le.text()
        if not temp:
            self.input1_C_le.clear()
            self.input1_C_le.insert('0')
        self.variable.input1_parameter = [float(self.input1_A_le.text()),float(self.input1_B_le.text()),float(self.input1_C_le.text())]
        
        #####
        temp=self.FRF_ref_le.text()
        if not temp:
            self.variable.ref_db = 1
            self.FRF_ref_le.clear()
            self.FRF_ref_le.insert('1')
        else:
            self.variable.ref_db = float(self.FRF_ref_le.text())

        temp=self.average_le.text()
        if not temp:
            self.variable.nb_mean = 10
            self.average_le.clear()
            self.average_le.insert('10')
        else:
            if str(self.average_le.text()) == 'Number of Average':
                 self.variable.nb_mean = 10
            else:
                self.variable.nb_mean = int(self.average_le.text())
        self.variable.time_range = int(self.time_range_cb.currentText())
        

    def define_delta_f(self):
        self.STOP_test=1
        self.test_button.setEnabled(True)
        
        try: # monitors undefined as analyseur attribute for initialization
            self.monitorL.reset(self.variable.input1_parameter[1]) # clean & define the left monitor plot
            self.monitorR.reset() # clean & define the right monitor plot
        except AttributeError:
            pass
            
        if float(self.frequency_le.text()) >= 400 and float(self.frequency_le.text()) < 17000 : # min freq and max sampling freq
            self.delta_f=float(self.frequency_le.text())/float(self.buffer_cb.currentText())
            self.delta_value.setText("{0:.3f}".format(self.delta_f))
        else:
            self.frequency_le.clear()
            self.frequency_le.insert('1000')
            self.delta_value.clear()
            self.delta_value.setText('Freq=[400Hz,17000Hz]')
            
        if float(self.frequency_le.text()) > 9000:
            buff = int(self.buffer_cb.currentText()) # size of buffer
            if buff < 400:
                self.delta_value.clear()
                self.delta_value.setText('FMax>9000Hz-->FFT line>=400')
                self.buffer_cb.setCurrentIndex(2)

    def set_window(self):
        self.STOP_test = 1
        self.test_button.setEnabled(True)
        
        try: # monitors undefined as analyseur attribute for initialization
            self.monitorL.reset(self.variable.input1_parameter[1]) # clean & define the left monitor plot
            self.monitorR.reset() # clean & define the right monitor plot
        except AttributeError:
            pass
            
        window_type=self.windowed_cb.currentText()
        if window_type=='Uniform':
            self.covering_label.setText('Covering (%)')
            self.covering_le.clear()
            self.covering_le.insert('0')
            self.covering_label.setVisible(False)
            self.covering_le.setEnabled(False)
            #####
            self.expo_label.setText('')
            self.expo_le.clear()
            self.expo_le.insert('')
            self.expo_label.setVisible(False)
            self.expo_le.setEnabled(False)
            #####
            self.covering2_label.setVisible(False)
            self.covering2_le.setEnabled(False)
            self.covering2_le.clear()
            self.covering2_le.insert('')
            
        if window_type=='Hanning':
            self.covering_label.setText('Covering (%)')
            self.covering_le.setEnabled(True)
            self.covering_le.clear()
            self.covering_le.insert("{0:.1f}".format(66))
            #####
            self.expo_label.setText('')
            self.expo_le.clear()
            self.expo_le.insert('')
            self.expo_label.setVisible(False)
            self.expo_le.setEnabled(False)
            #####
            self.covering2_label.setVisible(False)
            self.covering2_le.setEnabled(False)
            self.covering2_le.clear()
            self.covering2_le.insert('')
            
        if window_type=='Force/Exponential':
            self.covering_label.setText('')
            self.covering_le.clear()
            self.covering_le.insert('')
            self.covering_label.setVisible(False)
            self.covering_le.setEnabled(False)
            #####
            self.expo_label.setText('Exponential Time Constant (%)')
            self.expo_le.clear()
            self.expo_le.insert('25')
            self.expo_label.setVisible(True)
            self.expo_le.setEnabled(True)
            #####
            self.covering2_le.clear()
            self.covering2_label.setText('Force Time Constant (1-50%)')
            self.covering2_le.insert('10')
            self.covering2_label.setVisible(True)
            self.covering_label.setVisible(True)
            self.covering2_le.setEnabled(True)
            
    def set_input1_label(self):
        self.STOP_test=1
        self.test_button.setEnabled(True)
        
        try: # monitors undefined as analyseur attribute for initialization
            self.monitorL.reset(self.variable.input1_parameter[1]) # clean & define the left monitor plot
            self.monitorR.reset() # clean & define the right monitor plot
        except AttributeError:
            pass
        
        input_type=self.input1_cb.currentText()
        self.input1_A_le.clear()
        self.input1_B_le.clear()
        self.input1_C_le.clear()
        
        if input_type=='None':
            self.windowed_cb.setCurrentIndex(0)
            self.set_window()
            self.input1_A_le.insert('0')
            self.input1_B_le.insert('0')
            self.input1_C_le.insert('0')
            self.input1_A_label.setVisible(False)
            self.input1_B_label.setVisible(False)
            self.input1_C_label.setVisible(False)
            self.input1_A_le.setEnabled(False)
            self.input1_B_le.setEnabled(False)
            self.input1_C_le.setEnabled(False)
            #####
            self.hammer.setText('')
            self.accelerometer.setText('')
            #####
            self.average_le.clear()
            self.average_le.insert('10')
            self.average_le.setEnabled(True)
            self.average_label.setText('Number of Average')
            
        if input_type=='White noise':
            self.windowed_cb.setCurrentIndex(0)
            self.set_window()
            self.input1_A_label.setVisible(True)
            self.input1_B_label.setVisible(True)
            self.input1_C_label.setVisible(True)
            self.input1_A_le.setEnabled(True)
            self.input1_B_le.setEnabled(True)
            self.input1_C_le.setEnabled(True)
            #####
            self.input1_A_label.setText('Range (0 to 1)')
            self.input1_A_le.insert('1')
            self.input1_B_le.insert('0')
            self.input1_C_le.insert('0')
            self.input1_B_label.setVisible(False)
            self.input1_C_label.setVisible(False)
            self.input1_B_le.setEnabled(False)
            self.input1_C_le.setEnabled(False)
            #####
            self.hammer.setText('')
            self.accelerometer.setText('')
            #####
            self.average_le.clear()
            self.average_le.insert('10')
            self.average_le.setEnabled(True)
            self.average_label.setText('Number of Average')
            
        if input_type=='Sinus':
            self.windowed_cb.setCurrentIndex(0)
            self.set_window()
            self.input1_A_label.setVisible(True)
            self.input1_B_label.setVisible(True)
            self.input1_C_label.setVisible(True)
            self.input1_A_le.setEnabled(True)
            self.input1_B_le.setEnabled(True)
            self.input1_C_le.setEnabled(True)
            #####
            self.input1_A_label.setText('Frequency (Hz)')
            self.input1_A_le.insert('1000')
            self.input1_B_label.setText('Range (0 to 1)')
            self.input1_B_le.insert('1')
            self.input1_C_label.setText('Phase (degree)')
            self.input1_C_le.insert('0')
            #####
            self.hammer.setText('')
            self.accelerometer.setText('')
            #####
            self.average_le.clear()
            self.average_le.insert('10')
            self.average_le.setEnabled(True)
            self.average_label.setText('Number of Average')
            
        if input_type=='Impulse':
            # Force/exponential window automatically
            self.windowed_cb.setCurrentIndex(2)
            self.set_window()
            #####
            self.input1_A_label.setVisible(True)
            self.input1_B_label.setVisible(True)
            self.input1_C_label.setVisible(False)
            self.input1_A_le.setEnabled(True)
            self.input1_B_le.setEnabled(True)
            self.input1_C_le.setEnabled(False)
            #####
            self.input1_A_label.setText('Pre-Triggering (% of window)')
            self.input1_A_le.insert('5')
            self.input1_B_label.setText('Threshold')
            self.input1_B_le.insert('0.1')
            self.input1_C_le.insert('0')
            #####
            self.hammer.setText('Hammer')
            self.accelerometer.setText('Accelerometer')
            #####
            self.average_le.clear()
            self.average_le.insert('Number of Average')
            self.average_le.setEnabled(False)
            self.average_label.clear()

    def save_FRF(self):
        filename = QtGui.QFileDialog.getSaveFileName(self,'saveFile','FRF.txt')
        try:
            fid = open(filename[0],'w')
            line = 'Coherence, PSD (amplitude) (Units²/Hz), FRF (complex) (Units/Hz)\n'
            fid.write(line)
            fid.write("{:^40}".format('Frequency') + ',' + 
                      "{:^40}".format('Coherence') + ',' +
                      "{:^40}".format('PSD_channel1') + ',' + 
                      "{:^40}".format('PSD_channel2') + ',' + 
                      "{:^40}".format('FRF1/2_H1') + ',' + 
                      "{:^40}".format('FRF2/1_H1') + ',' + 
                      "{:^40}".format('FRF1/2_H2') + ',' + 
                      "{:^40}".format('FRF2/1_H2)') + '\n')
        
            freq = self.variable.data_freq
    
            for ii in range(0,np.size(freq)):
                fid.write("{:40.2f}".format(freq[ii]) + ',' +
                          "{:40.4E}".format(self.coherence[ii]) + ',' +
                          "{:40.4E}".format(self.autospectre[ii,0]) + ',' + 
                          "{:40.4E}".format(self.autospectre[ii,1]) + ',' + 
                          "{:40.4E}".format(self.FRF12[ii,0]) + ',' + 
                          "{:40.4E}".format(self.FRF21[ii,0]) + ',' + 
                          "{:40.4E}".format(self.FRF12[ii,1]) + ',' + 
                          "{:40.4E}".format(self.FRF21[ii,1]) + '\n')
            fid.close()
        # If you cancel the save action 
        except FileNotFoundError:
            pass


    def plot_FRF(self,autospectre,FRF12,FRF21,coherence,freq):
        self.auto.update_data_dB(freq,autospectre[:,0],autospectre[:,1],self.variable.ref_db**2,'Frequency (Hz)', 'Amplitude (dB)') # comparison reference also squared

        self.cohe.update_data(freq,100*coherence) # -> %
        if self.FRF_box.currentIndex()==0:
            data=FRF12[:,0]
        if self.FRF_box.currentIndex()==1:
            data=FRF21[:,0]
        if self.FRF_box.currentIndex()==2:
            data=FRF12[:,1]
        if self.FRF_box.currentIndex()==3:
            data=FRF21[:,1]
            
        if self.FRF_MP_box.isChecked():
            if self.FRF_db_box.isChecked():
                self.frfa.update_data_dB(freq,np.absolute(data),self.variable.ref_db,'Frequency (Hz)', 'Magnitude (dB)')
                self.frfb.update_data(freq,np.angle(data)*180/np.pi,'Frequency (Hz)', 'Phase (deg)')

            else:
                self.frfa.update_data(freq,np.absolute(data),'Frequency (Hz)', 'Magnitude (units)')
                self.frfb.update_data(freq,np.angle(data)*180/np.pi,'Frequency (Hz)', 'Phase (deg)')
                
        elif self.FRF_RI_box.isChecked():
            self.frfa.update_data(freq,np.real(data),'Frequency (Hz)', 'Real (units)')
            self.frfb.update_data(freq,np.imag(data),'Frequency (Hz)', 'Imag (units)')

    def stop_test(self):
        self.STOP_test = 1
        self.test_button.setEnabled(True)
        self.stop_test_button.setEnabled(False)

    def stop_mesure(self):
        self.STOP_mesure = 1
        self.start_button.setEnabled(True)
        self.stop_button.setEnabled(False)
        
    def reject_measure(self):
        self.measure_reject = True
    
    def read_callback(self,in_data, frame_count, time_info, status):
        ''' Function called for get the data for the read_stream'''
        if frame_count == self.BUFFER:
            self.signal_temp=np.reshape(np.frombuffer(in_data, 'float32'),(int(frame_count),2))
        return (in_data, pyaudio.paContinue)

    def write_callback(self,out_data, frame_count, time_info, status):
        ''' Function called for get the data for the write_stream'''
        out_data = self.signal_input.astype(np.float32).tostring()
        return (out_data, pyaudio.paContinue)


    def test_main(self):
        ##### initialize #####
        self.get_all_variable()
        self.actu_time_range() # get the indices to plot
        self.test_button.setEnabled(False)
        self.stop_test_button.setEnabled(True)
        self.monitorL.reset(self.variable.input1_parameter[1])
        self.monitorR.reset()
        
        ##### type of signal #####
        Impulse_activated = self.variable.input1_type=='Impulse'
        Sinus_activated =  self.variable.input1_type=='Sinus'
        WhiteNoise_activated = self.variable.input1_type=='White noise'
        
        ##### 
        RATE = int(self.variable.ratio*self.variable.freq_max)
        self.BUFFER = int(self.variable.ratio*self.variable.buffer_size)
        
        if Impulse_activated:
            buff=self.BUFFER
            time2 = np.arange(2*self.BUFFER)/float(RATE)
            pretrigg = int(float(self.variable.input1_parameter[0]*buff)/100)
            poids_expo = np.zeros((2*self.BUFFER))
            poids_force = np.zeros((2*self.BUFFER))
        time = np.arange(self.BUFFER)/float(RATE)
        self.signal_input = np.zeros((self.BUFFER,2))
        self.signal_temp = np.zeros((self.BUFFER,2))
        
        ##### define the buffers #####
        self.signal_tempL=signal_BUFF()
        self.signal_tempL.init(2*self.BUFFER)
        self.signal_tempR=signal_BUFF()
        self.signal_tempR.init(2*self.BUFFER)
        
        #######################
        # streams declaration #
        #######################
        signal_test=pyaudio.PyAudio()
        stream_read = signal_test.open(format=pyaudio.paFloat32,
                                       channels=2,
                                       rate=RATE,
                                       input=True,
                                       output=False,
                                       frames_per_buffer=self.BUFFER,
                                       stream_callback=self.read_callback)
        
        if Sinus_activated or WhiteNoise_activated:
            stream_write = signal_test.open(format=pyaudio.paFloat32,
                                            channels=2,
                                            rate=RATE,
                                            input=False,
                                            output=True,
                                            frames_per_buffer=self.BUFFER,
                                            stream_callback=self.write_callback)

        ###### initialize signal for White noise #####
        if WhiteNoise_activated:
            nb_mean = int(round(5/float(time[-1]))) # number of periods for 10s
            sig_in = self.variable.input1_parameter[0]*np.random.uniform(-1,1,nb_mean*self.BUFFER)
            sig_in = np.reshape(sig_in,(nb_mean,self.BUFFER))
            end = 0
        ###### initialize signal for Sinus #####  
        if Sinus_activated:
            nb_mean = int(round(5/float(time[-1]))) # number of periods for 10s
            tempo = np.arange(nb_mean*self.BUFFER)/float(RATE)
            sig_in = self.variable.input1_parameter[1]*(np.sin(2*np.pi*self.variable.input1_parameter[0]*tempo+(self.variable.input1_parameter[2]*np.pi/float(180))))
            sig_in = np.reshape(sig_in,(nb_mean,self.BUFFER))
            end = 0
            
        ###########
        # Display #
        ###########
        self.STOP_test = 0
        while self.STOP_test == 0:
            
            ##### to quit if the window has been closed #####
            app.aboutToQuit.connect(lambda : self.closeEvent)
            
            ##### start streams #####
            stream_read.start_stream()
            if Sinus_activated or WhiteNoise_activated:
                stream_write.start_stream()
                
            QtGui.QApplication.processEvents() # Refresh
            ##### pause for one buffer to be recorded #####
            if Impulse_activated:
                temps.sleep(0.6*time[-1]) 
            else:
                temps.sleep(0.3*time[-1]) 
            
            ##### Adding rotating buffer #####
            self.signal_tempL.add((1/float(self.variable.channel1_sensitivity))*self.signal_temp[:,0])
            self.signal_tempR.add((1/float(self.variable.channel2_sensitivity))*self.signal_temp[:,1])

            ##### Impulse case #####
            if Impulse_activated:
                
                self.monitorL.update_data_impulse(time2,self.signal_tempL.data,poids_force)
                self.monitorR.update_data_impulse(time2,self.signal_tempR.data,poids_expo)
                
                ##################################################
                # impulse detection & verification of the signal #
                ##################################################
                indice = np.array(np.where(self.signal_tempL.data>self.variable.input1_parameter[1])).astype('int')[0]
                if np.size(indice) > 0 and (indice[0]-pretrigg) > 0 and (indice[0]-pretrigg+buff) < 2*self.BUFFER:
                    poids_expo[indice[0]-pretrigg:indice[0]-pretrigg+buff] = np.exp(-np.arange(buff)/float(self.variable.exponential*buff))*max(abs(self.signal_tempR.data))
                    
                    ##### calculation poids force window
                    n_max = 2*int(buff*self.variable.covering2)
                    for ii in range(0,2*int(buff*self.variable.covering2)): # force window returns to 0 after 2*force time constant
                        if ii < int(buff*self.variable.covering2):
                            poids_force[indice[0]-pretrigg+ii] = 1*max(abs(self.signal_tempL.data))
                        else:
                          poids_force[indice[0]-pretrigg+ii] = 0.5*(1+np.cos(np.pi*(ii+0.5*n_max)/((1-0.5)*n_max)))*max(abs(self.signal_tempL.data))
                          
                    self.monitorL.update_data_impulse(time2,self.signal_tempL.data,poids_force)
                    self.monitorR.update_data_impulse(time2,self.signal_tempR.data,poids_expo)
                    self.stop_test()
                
            ##### Sinus, White noise or None case #####
            else:      
                
                ###### input signal (for Sinus & White noise) ##### 
                if Sinus_activated or WhiteNoise_activated:
                    self.signal_input[:,0] = sig_in[end,:]
                    end += 1
                    if end == nb_mean:
                        end = 0
                        
                ##### plot recorded signals #####
                self.monitorL.update_data(time[self.index_signal],self.signal_tempL.data[self.index_signal])
                self.monitorR.update_data(time[self.index_signal],self.signal_tempR.data[self.index_signal])
                
            ##### Refresh #####
            QtGui.QApplication.processEvents() 
            
        ##### stop streams & signal #####
        stream_read.stop_stream()
        stream_read.close()
        if Sinus_activated or WhiteNoise_activated:
            stream_write.stop_stream()
            stream_write.close
        signal_test.terminate()


    def measureFRF(self):
        self.stop_test()
        self.get_all_variable()
        self.start_button.setEnabled(False)
        self.stop_button.setEnabled(True)
        self.save_button.setEnabled(False)
        
        ##### refresh #####
        QtGui.QApplication.processEvents()
        self.frfa.reset() # put back the hori and verti lines at 0
        self.frfb.reset()
        self.auto.reset()
        
        ##### init variable #####
        freq = self.variable.data_freq # frequency grid
        freq_size = np.size(freq)
        delta_freq = float(self.variable.delta_freq)
        self.autospectre = 1e-7*np.ones((freq_size,2),)
        self.interspectre = 1e-7*np.ones((freq_size,2),dtype=complex)
        self.FRF12 = 1e-7*np.ones((freq_size,3),dtype=complex)
        self.FRF21 = 1e-7*np.ones((freq_size,3),dtype=complex)
        self.coherence = 1e-7*np.ones(freq_size)
        autospectre_save = np.copy(self.autospectre)
        interspectre_save = np.copy(self.interspectre)
        FRF12_save = np.copy(self.FRF12)
        FRF21_save = np.copy(self.FRF21)
        coherence_save = np.copy(self.coherence)
        measure_already_rejected = False
        
        ##################
        # Initialization #
        ##################
        Impulse_activated = self.variable.input1_type=='Impulse'
        Sinus_activated =  self.variable.input1_type=='Sinus'
        WhiteNoise_activated = self.variable.input1_type=='White noise'
        
        RATE = int(self.variable.ratio*self.variable.freq_max)
        self.BUFFER = int(self.variable.ratio*self.variable.buffer_size)
        buff = self.BUFFER
        
        if Impulse_activated:
            # if hammer, buffer size is doubled in order to have enough point to take a complete regular buffer
            pretrigg = int(float(self.variable.input1_parameter[0]*buff)/100)
            poids_expo = np.zeros((buff))
            poids_expo = np.exp(-np.arange(buff)/float(self.variable.exponential*buff))
            poids_force = np.zeros((buff))
            n_max = 2*int(buff*self.variable.covering2)
            for ii in range(0,2*int(buff*self.variable.covering2)): # force window returns to 0 after 2*force time constant
                if ii < int(buff*self.variable.covering2):
                    poids_force[ii] = 1
                else:
                    poids_force[ii] = 0.5*(1+np.cos(np.pi*(ii+0.5*n_max)/((1-0.5)*n_max)))            
                    
        self.signal_input = np.zeros((buff,2))
        self.signal_temp = np.zeros((buff,2))
        signal_tempL = signal_BUFF()
        signal_tempL.init(2*buff)
        signal_tempR = signal_BUFF()
        signal_tempR.init(2*buff)
        time = buff/float(RATE) # recording time for a buffer
        data_fftL = np.zeros(buff,dtype=complex)
        data_fftR = np.zeros(buff,dtype=complex)
        self.FFT_out = np.zeros((buff,2),dtype=complex)
        
        #######################
        # streams declaration #
        #######################
        signal_test=pyaudio.PyAudio()
        
        stream_read = signal_test.open(format=pyaudio.paFloat32,
                                       channels= 2,
                                       rate=RATE,
                                       input=True,
                                       output=False,
                                       frames_per_buffer=buff,
                                       stream_callback=self.read_callback)
        
        if Sinus_activated or WhiteNoise_activated:
            stream_write = signal_test.open(format=pyaudio.paFloat32,
                                            channels= 2,
                                            rate=RATE,
                                            input=False,
                                            output=True,
                                            frames_per_buffer=buff,
                                            stream_callback=self.write_callback)
            
        ############################
        # Definition of parameters #
        ############################
        #####  define poids for window #####
        if self.variable.window=='Uniform' :
            poids = np.ones((buff))
        if self.variable.window=='Hanning' :
            poids = 0.5-0.5*(np.cos(2*np.pi*np.arange(0,buff)/buff))
            
        ##### indices for the covering #####
        taux_overlap = self.variable.covering
        nb_cover = int(round(1/(1-taux_overlap)))
        ind = np.zeros((buff,nb_cover),dtype=int)
        for j in range(0,nb_cover):
            ind[:,j] = np.arange(j*(1-taux_overlap)*buff,j*(1-taux_overlap)*buff+buff).astype(int)
        self.nb_make.display('0')
        
        ############
        # MEASURES #
        ############
        self.STOP_mesure = 0
        while self.STOP_mesure == 0:
            ii = 0   # Number of measures
            kk = 0.0 # Take into the covering
            
            ##### to quit if the window has been closed #####
            app.aboutToQuit.connect(lambda : self.closeEvent)
            
            ##### Launch streams #####
            stream_read.start_stream()
            if Sinus_activated or WhiteNoise_activated:
                stream_write.start_stream()

            ##### gereration of the sinus signal & cutting in number of means #####
            if WhiteNoise_activated:
                sig_in = self.variable.input1_parameter[0]*np.random.uniform(-1,1,self.variable.nb_mean*buff)
                sig_in = np.reshape(sig_in,(self.variable.nb_mean,buff))
                
            if Sinus_activated:
                tempo=np.arange(self.variable.nb_mean*buff)/float(RATE)
                sig_in = self.variable.input1_parameter[1]*(np.sin(2*np.pi*self.variable.input1_parameter[0]*tempo+(self.variable.input1_parameter[2]*np.pi/float(180))))
                sig_in = np.reshape(sig_in,(self.variable.nb_mean,buff))
                                
            ##### Impulse case #####
            if Impulse_activated:
                ##### Average calculation #####
                while not(self.STOP_mesure):
                    self.stop_button.setEnabled(True)
                    
                    ##### to quit if the window has been closed #####
                    app.aboutToQuit.connect(lambda : self.closeEvent)
                    
                    ##### Adding rotating buffer #####
                    temps.sleep(0.6*time) # Pause to record a buffer
                    signal_tempL.add((1/float(self.variable.channel1_sensitivity))*self.signal_temp[:,0])
                    signal_tempR.add((1/float(self.variable.channel2_sensitivity))*self.signal_temp[:,1])
                    
                    ##### define the indices #####
                    indice = np.array(np.where(signal_tempL.data>self.variable.input1_parameter[1])).astype('int')[0]
                    
                    ##### Measure triggered #####
                    if np.size(indice) > 0 and (indice[0]-pretrigg) > 0 and (indice[0]-pretrigg+buff) < 2*buff:
                        
                        ##### set buttons #####
                        self.stop_button.setEnabled(False)
                        measure_already_rejected = False
                        
                        ##### incrementation #####
                        ii += 1
                        kk += 1
                        
                        ##### save the previous data (in case of rejection) #####
                        autospectre_save = np.copy(self.autospectre)
                        interspectre_save = np.copy(self.interspectre)
                        FRF12_save = np.copy(self.FRF12)
                        FRF21_save = np.copy(self.FRF21)
                        coherence_save = np.copy(self.coherence)
                        
                        ##### FFT #####
                        data_fftL = 2*np.fft.fft(fenetre(np.multiply(poids_force,poids_expo),signal_tempL.data[indice[0]-pretrigg:indice[0]-pretrigg+buff]),buff)/float(buff)
                        data_fftR = 2*np.fft.fft(fenetre(poids_expo,signal_tempR.data[indice[0]-pretrigg:indice[0]-pretrigg+buff]),buff)/float(buff)
                        data_fftL[0] = 0.5*data_fftL[0] # the continued component isn't multiplied by 2
                        data_fftR[0] = 0.5*data_fftR[0] # the continued component isn't multiplied by 2
                        
                        ##### autospectrum #####
                        self.autospectre[:,0] = abs((kk-1)*self.autospectre[:,0] + autospectrum(data_fftL[0:freq_size]/delta_freq)) / kk # calculation in PSD --> divide by sampling freq + average 
                        self.autospectre[:,1] = abs((kk-1)*self.autospectre[:,1] + autospectrum(data_fftR[0:freq_size]/delta_freq)) / kk # calculation in PSD --> divide by sampling freq + average 
                        np.where(self.autospectre < 1e-7, self.autospectre, 1e-7) # Prevent the 0 value
                        
                        ##### interspectrum #####
                        self.interspectre[:,0] = ((kk-1)*self.interspectre[:,0] + intspectrum(data_fftL[0:freq_size]/delta_freq,data_fftR[0:freq_size]/delta_freq)) / kk
                        self.interspectre[:,1] = ((kk-1)*self.interspectre[:,1] + intspectrum(data_fftR[0:freq_size]/delta_freq,data_fftL[0:freq_size]/delta_freq)) / kk
                        np.where(self.interspectre < 1e-7, self.interspectre, 1e-7) # Prevent the 0 value
                        
                        ##### coherence #####
                        self.coherence = abs(self.interspectre[:,0]) / np.sqrt(self.autospectre[:,0]*self.autospectre[:,1])
                        
                        ##### FRF #####
                        self.FRF12[:,0] = self.interspectre[:,0] / self.autospectre[:,0]
                        self.FRF12[:,1] = self.autospectre[:,1] / self.interspectre[:,1]
                        self.FRF21[:,0] = self.interspectre[:,1] / self.autospectre[:,1]
                        self.FRF21[:,1] = self.autospectre[:,0] / self.interspectre[:,0]
                        
                        ##### display FRF #####
                        self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,freq)
                        QtGui.QApplication.processEvents() # Refresh
                        
                        ###### Put back buffers to 0 #####
                        signal_tempL.reset()
                        signal_tempR.reset()
                        self.signal_temp = np.zeros((buff,2))
                        indice = []
                    
                    ##### set buttons #####
                    self.stop_button.setEnabled(True)
                    if ii > 0 and not(measure_already_rejected):
                        self.reject_button.setEnabled(True)
                    QtGui.QApplication.processEvents() # Refresh
                    
                    ##### reject the measure #####
                    if self.measure_reject:
                        ##### state the rejection #####
                        self.measure_reject = False
                        measure_already_rejected = True
                        ##### set the reject button #####
                        self.reject_button.setEnabled(False)
                        ##### decrementation #####
                        ii -= 1
                        kk -= 1
                        ##### take back the previous values ######
                        self.autospectre = np.copy(autospectre_save)
                        self.interspectre = np.copy(interspectre_save)
                        self.FRF12 = np.copy(FRF12_save)
                        self.FRF21 = np.copy(FRF21_save)
                        self.coherence = np.copy(coherence_save)
                        ##### plot the previous graphs #####
                        self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,freq)
                        QtGui.QApplication.processEvents() # Refresh
                        
                    self.nb_make.display(ii)
                    
            ##### Sinus, White noise or None case #####
            else:
                ##### Average calculation #####
                while ii < self.variable.nb_mean and self.STOP_mesure == 0:
                    
                    ##### input signal (for Sinus or White noise) #####
                    if Sinus_activated or WhiteNoise_activated:
                        self.signal_input[:,0] = sig_in[ii,:]
                        
                    ##### incrementation #####
                    ii += 1
                    
                    ##### Pause to record a buffer #####
                    temps.sleep(0.3*time)
                    
                    ##### Adding rotating buffer #####
                    signal_tempL.add((1/float(self.variable.channel1_sensitivity))*self.signal_temp[:,0])
                    signal_tempR.add((1/float(self.variable.channel2_sensitivity))*self.signal_temp[:,1])
                    
                    ##### calculation of FFT, autospectrum, coherence, and FRF #####
                    for jj in range(nb_cover):
                        
                        ##### incrementation #####
                        kk += 1
                        
                        ##### FFT #####
                        data_fftL = 2*np.fft.fft(fenetre(poids,signal_tempL.data[ind[:,jj]]),buff)/float(buff)
                        data_fftR = 2*np.fft.fft(fenetre(poids,signal_tempR.data[ind[:,jj]]),buff)/float(buff)
                        data_fftL[0] = 0.5*data_fftL[0] # the continued component isn't multiplied by 2
                        data_fftR[0] = 0.5*data_fftR[0] # the continued component isn't multiplied by 2
                        
                        ##### autospectrum #####
                        self.autospectre[:,0] = abs((kk-1)*self.autospectre[:,0] + autospectrum(data_fftL[0:freq_size]/delta_freq)) / kk # calculation in PSD --> divide by sampling freq + average 
                        self.autospectre[:,1] = abs((kk-1)*self.autospectre[:,1] + autospectrum(data_fftR[0:freq_size]/delta_freq)) / kk # calculation in PSD --> divide by sampling freq + average 
                        np.where(self.autospectre < 1e-7, self.autospectre, 1e-7) # Prevent the 0 value
                        
                        ##### interspectrum #####
                        self.interspectre[:,0] = ((kk-1)*self.interspectre[:,0] + intspectrum(data_fftL[0:freq_size]/delta_freq,data_fftR[0:freq_size]/delta_freq))/ kk
                        self.interspectre[:,1] = ((kk-1)*self.interspectre[:,1] + intspectrum(data_fftR[0:freq_size]/delta_freq,data_fftL[0:freq_size]/delta_freq)) / kk
                        np.where(self.interspectre < 1e-7, self.interspectre, 1e-7) # Prevent the 0 value
                        
                        ##### coherence #####
                        self.coherence = abs(self.interspectre[:,0]) / np.sqrt(self.autospectre[:,0]*self.autospectre[:,1])
                        
                        ##### FRF #####
                        self.FRF12[:,0] = self.interspectre[:,0] / self.autospectre[:,0]
                        self.FRF12[:,1] = self.autospectre[:,1] / self.interspectre[:,1]
                        self.FRF21[:,0] = self.interspectre[:,1] / self.autospectre[:,1]
                        self.FRF21[:,1] = self.autospectre[:,0] / self.interspectre[:,0]
                        
                    ##### display graphs ##### 
                    self.plot_FRF(self.autospectre,self.FRF12,self.FRF21,self.coherence,freq)
                    QtGui.QApplication.processEvents() # Refresh
                    self.nb_make.display(int(ii))
            
            ##### reset #####
            self.frfa.reset() # put back hori & verti lines at 0
            self.frfb.reset() # put back hori & verti lines at 0
            self.auto.reset()
            
            #### stop measure & streams #####
            self.stop_mesure()
            stream_read.stop_stream()
            stream_read.close()
            if Sinus_activated or WhiteNoise_activated:
                stream_write.stop_stream()
                stream_write.close 
            signal_test.terminate()
            
            ##### set buttons #####
            self.save_button.setEnabled(True)
            self.reject_button.setEnabled(False)
            
            ##### Refresh #####
            QtGui.QApplication.processEvents() 
            


###############################################################################
############################## MAIN METHOD ####################################
###############################################################################
            
if __name__=='__main__':
    '''
    Manage the whole program
    '''
    ##### Creation of a QApplication object #####
    app = QtGui.QApplication(sys.argv)
    
    ##### Define the style of the object #####
    style = QtGui.QStyleFactory.create('Plastique')
    app.setStyle(style)
    
    ##### Display the opening screen #####
    splash = splashScreen()
    splash.show_splash()
    app.processEvents()
    QtCore.QThread.sleep(3)
    splash.close_splash()
    
    ##### Launch the interface #####
    gui = analyseur()
    gui.show()
    
    ##### Main loop for events #####
    app.exec_()
    
    ##### Close the program #####
    me = os.getpid()
    kill_proc_tree(me)