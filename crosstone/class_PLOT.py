
###############################################################################
################################### Imports ###################################
###############################################################################
import numpy
import pyqtgraph as pg
from PyQt5 import QtGui,QtCore


###############################################################################
################################### Classes ###################################
###############################################################################

##############################################################
##### Class to plot the LEFT graph of the TEST interface #####
##############################################################
class plot_monitorL:
    
    def __init__(self,p,val):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
        self.p.setBackground((255,255,255))
        self.courbe1=self.p.plot()
        self.courbe1.setData(numpy.linspace(0,10,50),numpy.zeros((50)),pen=pg.mkPen((25,25,112),width=2))
        self.courbe5=self.p.plot()
        
        ##### axis shape #####
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='Amplitude (Unit)')
        self.p.setLabel('bottom',text ='Time (s)') 
        self.trigg = pg.InfiniteLine(angle=0, movable=True,pen=pg.mkPen(color=(0,128,0),width=2))
        self.p.addItem(self.trigg)
        self.trigg.hide()
        self.trigg.setValue(val)
        
        
    def reset(self,val):
        self.p.clear()
        self.p.setBackground((255,255,255))
        pen = pg.mkPen(color='k', width=2)
        self.courbe1=self.p.plot()
        self.courbe1.setData(numpy.linspace(0,10,50),numpy.zeros((50)),pen=pg.mkPen((25,25,112),width=2))
        self.courbe3=self.p.plot()
        self.courbe5=self.p.plot()
        
        ##### axis shape #####
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='Amplitude (Unit)')
        self.p.setLabel('bottom',text ='Time(s)') 
        self.p.addItem(self.trigg)
        self.trigg.hide()
        self.trigg.setValue(val)
        
        
    def update_data(self,X,Y1):   
        ##### set data #####
        self.courbe1.setData(X,Y1,pen=pg.mkPen(color=(25,25,112),width=2),autoDownsample=True)
        
        ##### set range #####               
        self.p.setXRange(X[0],X[-1])
        self.p.setYRange(-1.1*numpy.maximum(max(abs(Y1)),0.2),1.1*numpy.maximum(max(abs(Y1)),0.2))


    def update_data_impulse(self,X,Y1,Y3): 
        ##### set data #####
        self.trigg.show()
        self.courbe1.setData(X,Y1,pen=pg.mkPen(color=(25,25,112),width=2),autoDownsample=True)                     
        self.courbe5.setData(X,Y3,pen=pg.mkPen(color=(138,0,0),width=2),autoDownsample=True)
        
        ##### set range #####
        self.p.setXRange(X[0],X[-1])
        self.p.setYRange(-1.1*numpy.maximum(max(abs(Y1)),0.2),1.1*numpy.maximum(max(abs(Y1)),0.2))


    def suppr(self):
        self.p.clear()


###############################################################
##### Class to plot the RIGHT graph of the TEST interface #####
###############################################################
class plot_monitorR:
    
    def __init__(self,p):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
        self.p.setBackground((255,255,255))
        self.courbe1=self.p.plot()
        self.courbe1.setData(numpy.linspace(0,10,50),numpy.zeros((50)),pen=pg.mkPen('r',width=2))
        self.courbe4=self.p.plot()
        
        ##### axis shape #####
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='Amplitude (Unit)')
        self.p.setLabel('bottom',text ='Time (s)')
        
        
    def reset(self):
        self.p.clear()
        pen = pg.mkPen(color='k', width=2)
        self.p.setBackground((255,255,255))
        self.courbe1=self.p.plot()
        self.courbe1.setData(numpy.linspace(0,10,50),numpy.zeros((50)),pen=pg.mkPen('r',width=2))
        self.courbe4=self.p.plot()
        
        ##### axis shape #####
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='Amplitude (Unit)')
        self.p.setLabel('bottom',text ='Time(s)') 
        
        
    def update_data(self,X,Y1):     
        ##### set data #####
        self.courbe1.setData(X,Y1,pen=pg.mkPen(color='r',width=2),autoDownsample=True)
        
        ##### set range #####           
        self.p.setXRange(X[0],X[-1])
        self.p.setYRange(-1.1*numpy.maximum(max(abs(Y1)),0.2),1.1*numpy.maximum(max(abs(Y1)),0.2))
        
        ##### set caption #####
        self.l=self.p.getPlotItem().addLegend([150,60],[800,10])
        self.l.addItem(self.courbe1,'Channel 1')
         
        
    def update_data_impulse(self,X,Y1,Y2):          
        ##### set data #####
        self.courbe1.setData(X,Y1,pen=pg.mkPen(color='r',width=2),autoDownsample=True)  
        self.courbe4.setData(X,Y2,pen=pg.mkPen(color='y',width=2),autoDownsample=True) 
        
        ##### set range #####                       
        self.p.setXRange(X[0],X[-1])
        self.p.setYRange(-1.1*numpy.maximum(max(abs(Y1)),0.2),1.1*numpy.maximum(max(abs(Y1)),0.2))
        
        ##### set caption #####
        self.l=self.p.getPlotItem().addLegend([150,60],[800,10])
        self.l.addItem(self.courbe1,'Channel 1')
        self.l.addItem(self.courbe4,'Exponential Window')
         
        
    def suppr(self):
        self.p.clear()


################################################################
##### Class to plot the COHERENCE of the MEASURE interface #####
################################################################
class plotco:
    
    def __init__(self,p,label1,label2):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
        self.p.setBackground((255,255,255))
        self.courbe1=self.p.plot()
        self.courbe1.setData(numpy.linspace(0,1,50),numpy.zeros((50)),pen=pg.mkPen('b',width=2))
        
        ##### axis shape #####
        self.p.setXRange(0,2)
        self.p.setYRange(-1,100)
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('left').setStyle(autoExpandTextSpace=False)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text=label2)
        self.p.setLabel('bottom',text=label1) 
        
        
    def update_data(self,X,Y):  
        ##### set data #####        
        self.courbe1.setData(X,Y,pen=pg.mkPen(color='b',width=2),autoDownsample=True) 
        
        ##### set range #####            
        self.p.setXRange(0,X[-1])
        self.p.setYRange(-1,100)
         
        
    def suppr(self):
        self.p.clear()


###################################################################################
##### Class to plot the Power Spectral Density (PSD) of the MEASURE interface #####
###################################################################################
class plot_autospectrum:
    
    def __init__(self,p,label1,label2):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
        self.p.setBackground((255,255,255))
        self.courbe1=self.p.plot()
        self.courbe1.setData(numpy.linspace(0,10,50),numpy.zeros((50)),pen=pg.mkPen('b',width=2))
        self.courbe2=self.p.plot()
        self.courbe2.setData(numpy.linspace(0,10,50),numpy.zeros((50)),pen=pg.mkPen('r',width=2))
        
        ##### axis shape #####
        self.p.autoRange()
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text =label2)
        self.p.setLabel('bottom',text =label1)
        
        ##### adding visualization lines #####
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen(color=(0,128,0),width=1))
        self.hLine = pg.InfiniteLine(angle=0, movable=True,pen=pg.mkPen(color=(0,128,0),width=1))
        self.vtext=pg.TextItem() 
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.hLine, ignoreBounds=True)
        self.p.addItem(self.vtext)
        font=QtGui.QFont('AnyStyle',pointSize=9,weight=99)
        self.vtext.setFont(font)
        self.update_cross()
        
        ##### set caption #####
        self.l=self.p.getPlotItem().addLegend([100,60],[150,10])
        self.l.addItem(self.courbe1,'Channel 1')
        self.l.addItem(self.courbe2,'Channel 2')
        
        
    def reset(self):
        ##### define max ranges #####
        x_min=(self.p.getViewBox().viewRange()[0][0])
        x_max=(self.p.getViewBox().viewRange()[0][1])
        y_min=(self.p.getViewBox().viewRange()[1][0])
        y_max=(self.p.getViewBox().viewRange()[1][1])
        
        ##### set horizontal & vertical lines #####
        self.vLine.setValue(0.5*(x_max+x_min))
        self.hLine.setValue(0.5*(y_max+y_min))
        
        ##### refresh #####
        self.update_cross()
        
        
    def update_cross(self):
        ##### define max ranges #####
        x_min = 0.9*(self.p.getViewBox().viewRange()[0][0])
        x_max = 0.98*(self.p.getViewBox().viewRange()[0][1])
        y_min = 0.9*(self.p.getViewBox().viewRange()[1][0])
        y_max = 0.98*(self.p.getViewBox().viewRange()[1][1])
        
        ##### get values #####
        ind_x = self.vLine.value()
        ind_y = self.hLine.value()
        ##### set the lines #####
        self.vLine.setValue(max(ind_x, x_min))
        self.hLine.setValue(max(ind_y, y_min))
        self.vLine.setValue(min(ind_x, x_max))
        self.hLine.setValue(min(ind_y, y_max))
        ##### set the displayed value #####
        self.vtext.setText(text="{0:.1f}".format(ind_x)+';'+"{0:.1E}".format(ind_y),color=(0,0,0))
        
        ##### get values #####
        x_pos = ind_x
        y_pos = ind_y   
        ##### pick a good positionfor the text #####
        if ind_x > 0.8*x_max:
            x_pos = ind_x-0.2*(x_max-x_min)
        if ind_y < 0.7*y_min:
            y_pos = ind_y+0.1*(y_max-y_min)
        ##### set the position of the text #####
        self.vtext.setPos(x_pos,y_pos)
         
        
    def update_data_dB(self,X,Y1,Y2,ref,label1,label2): 
        ##### set data #####
        self.courbe1.setData(X,10*numpy.log10(Y1/float(ref)),pen=pg.mkPen(color='b',width=2),autoDownsample=True)        
        self.courbe2.setData(X,10*numpy.log10(Y2/float(ref)),pen=pg.mkPen(color='r',width=2),autoDownsample=True)
        
        ##### axis range #####
        self.p.autoRange()
        
        ##### set caption #####
        self.p.setLabel('left', text =label2)
        self.p.setLabel('bottom',text =label1)
         
        
    def suppr(self):
        self.p.clear()


##########################################################
##### Class to plot the FRF of the MEASURE interface #####
##########################################################
class plot2D:
    
    def __init__(self,p,label1,label2):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
        self.p.setBackground((255,255,255))
        self.courbe=self.p.plot()
        self.courbe.setData(numpy.linspace(0,10,50),numpy.zeros((50)),pen=pg.mkPen('b',width=2))
        
        ##### axis shape #####
        self.p.autoRange()
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text =label2)
        self.p.setLabel('bottom',text =label1)
        
        ##### adding visualization lines #####
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen(color=(0,128,0),width=1))
        self.hLine = pg.InfiniteLine(angle=0, movable=True,pen=pg.mkPen(color=(0,128,0),width=1))
        self.vtext=pg.TextItem() 
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.hLine, ignoreBounds=True)
        self.p.addItem(self.vtext)
        font=QtGui.QFont('AnyStyle',pointSize=9,weight=99)
        self.vtext.setFont(font)
        self.update_cross()
        
        
    def reset(self):
        ##### define max ranges #####
        x_min=(self.p.getViewBox().viewRange()[0][0])
        x_max=(self.p.getViewBox().viewRange()[0][1])
        y_min=(self.p.getViewBox().viewRange()[1][0])
        y_max=(self.p.getViewBox().viewRange()[1][1])
        
        ##### set horizontal & vertical lines #####
        self.vLine.setValue(0.5*(x_max+x_min))
        self.hLine.setValue(0.5*(y_max+y_min))
        
        ##### refresh #####
        self.update_cross()
        
        
    def update_cross(self):
        ##### define max ranges #####
        x_min = 0.9*(self.p.getViewBox().viewRange()[0][0])
        x_max = 0.98*(self.p.getViewBox().viewRange()[0][1])
        y_min = 0.9*(self.p.getViewBox().viewRange()[1][0])
        y_max = 0.98*(self.p.getViewBox().viewRange()[1][1])
        
        ##### get values #####
        ind_x = self.vLine.value()
        ind_y = self.hLine.value()
        ##### set the lines #####
        self.vLine.setValue(max(ind_x, x_min))
        self.hLine.setValue(max(ind_y, y_min))
        self.vLine.setValue(min(ind_x, x_max))
        self.hLine.setValue(min(ind_y, y_max))
        ##### set the displayed value #####
        self.vtext.setText(text="{0:.1f}".format(ind_x)+';'+"{0:.1E}".format(ind_y),color=(0,0,0))
        
        ##### get values #####
        x_pos = ind_x
        y_pos = ind_y   
        ##### pick a good positionfor the text #####
        if ind_x > 0.8*x_max:
            x_pos = ind_x-0.2*(x_max-x_min)
        if ind_y < 0.7*y_min:
            y_pos = ind_y+0.1*(y_max-y_min)
        ##### set the position of the text #####
        self.vtext.setPos(x_pos,y_pos)
         
        
    def update_data(self,X,Y,label1,label2): 
        ##### set data #####
        self.courbe.setData(X,Y,pen=pg.mkPen(color='b',width=2),autoDownsample=True)    
        
        ##### set range #####
        self.p.autoRange()
        
        ##### set caption #####
        self.p.setLabel('left', text=label2)
        self.p.setLabel('bottom',text=label1)
    
    
    def update_data_dB(self,X,Y,ref,label1,label2): 
        ##### set data #####
        self.courbe.setData(X,20*numpy.log10(Y/ref),pen=pg.mkPen(color='b',width=2),autoDownsample=True) 
        
        ##### set range #####
        self.p.autoRange()
        
        ##### set caption #####
        self.p.setLabel('left', text=label2)
        self.p.setLabel('bottom',text=label1)
         
        
    def suppr(self):
        self.p.clear()

